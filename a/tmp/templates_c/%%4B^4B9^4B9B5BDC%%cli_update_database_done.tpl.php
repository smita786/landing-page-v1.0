<?php /* Smarty version 2.6.26, created on 2011-07-28 14:33:18
         compiled from /var/www/a/plugins/CoreUpdater/templates/cli_update_database_done.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('block', 'textformat', '/var/www/a/plugins/CoreUpdater/templates/cli_update_database_done.tpl', 1, false),array('modifier', 'translate', '/var/www/a/plugins/CoreUpdater/templates/cli_update_database_done.tpl', 2, false),array('modifier', 'unescape', '/var/www/a/plugins/CoreUpdater/templates/cli_update_database_done.tpl', 2, false),array('modifier', 'implode', '/var/www/a/plugins/CoreUpdater/templates/cli_update_database_done.tpl', 48, false),)), $this); ?>
<?php $this->_tag_stack[] = array('textformat', array()); $_block_repeat=true;smarty_block_textformat($this->_tag_stack[count($this->_tag_stack)-1][1], null, $this, $_block_repeat);while ($_block_repeat) { ob_start(); ?>
<?php $this->assign('helpMessage', ((is_array($_tmp=((is_array($_tmp='CoreUpdater_HelpMessageContent')) ? $this->_run_mod_handler('translate', true, $_tmp, '[', ']', "\n\n* ") : smarty_modifier_translate($_tmp, '[', ']', "\n\n* ")))) ? $this->_run_mod_handler('unescape', true, $_tmp) : smarty_modifier_unescape($_tmp))); ?>

<?php if ($this->_tpl_vars['coreError']): ?>
	[X] <?php echo ((is_array($_tmp=((is_array($_tmp='CoreUpdater_CriticalErrorDuringTheUpgradeProcess')) ? $this->_run_mod_handler('translate', true, $_tmp) : smarty_modifier_translate($_tmp)))) ? $this->_run_mod_handler('unescape', true, $_tmp) : smarty_modifier_unescape($_tmp)); ?>


	<?php $_from = $this->_tpl_vars['errorMessages']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['message']):
?>
		* <?php echo $this->_tpl_vars['message']; ?>


	<?php endforeach; endif; unset($_from); ?>

	<?php echo ((is_array($_tmp=((is_array($_tmp='CoreUpdater_HelpMessageIntroductionWhenError')) ? $this->_run_mod_handler('translate', true, $_tmp) : smarty_modifier_translate($_tmp)))) ? $this->_run_mod_handler('unescape', true, $_tmp) : smarty_modifier_unescape($_tmp)); ?>


	* <?php echo $this->_tpl_vars['helpMessage']; ?>



	<?php echo ((is_array($_tmp='CoreUpdater_ErrorDIYHelp')) ? $this->_run_mod_handler('translate', true, $_tmp) : smarty_modifier_translate($_tmp)); ?>


	* <?php echo ((is_array($_tmp='CoreUpdater_ErrorDIYHelp_1')) ? $this->_run_mod_handler('translate', true, $_tmp) : smarty_modifier_translate($_tmp)); ?>


	* <?php echo ((is_array($_tmp='CoreUpdater_ErrorDIYHelp_2')) ? $this->_run_mod_handler('translate', true, $_tmp) : smarty_modifier_translate($_tmp)); ?>


	* <?php echo ((is_array($_tmp='CoreUpdater_ErrorDIYHelp_3')) ? $this->_run_mod_handler('translate', true, $_tmp) : smarty_modifier_translate($_tmp)); ?>


	* <?php echo ((is_array($_tmp='CoreUpdater_ErrorDIYHelp_4')) ? $this->_run_mod_handler('translate', true, $_tmp) : smarty_modifier_translate($_tmp)); ?>


	* <?php echo ((is_array($_tmp='CoreUpdater_ErrorDIYHelp_5')) ? $this->_run_mod_handler('translate', true, $_tmp) : smarty_modifier_translate($_tmp)); ?>


<?php else: ?>
	<?php if (count ( $this->_tpl_vars['warningMessages'] ) > 0): ?>
		[!] <?php echo ((is_array($_tmp=((is_array($_tmp='CoreUpdater_WarningMessages')) ? $this->_run_mod_handler('translate', true, $_tmp) : smarty_modifier_translate($_tmp)))) ? $this->_run_mod_handler('unescape', true, $_tmp) : smarty_modifier_unescape($_tmp)); ?>


		<?php $_from = $this->_tpl_vars['warningMessages']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['message']):
?>
			* <?php echo $this->_tpl_vars['message']; ?>


		<?php endforeach; endif; unset($_from); ?>
	<?php endif; ?>
						
	<?php if (count ( $this->_tpl_vars['errorMessages'] ) > 0): ?>
		[X] <?php echo ((is_array($_tmp=((is_array($_tmp='CoreUpdater_ErrorDuringPluginsUpdates')) ? $this->_run_mod_handler('translate', true, $_tmp) : smarty_modifier_translate($_tmp)))) ? $this->_run_mod_handler('unescape', true, $_tmp) : smarty_modifier_unescape($_tmp)); ?>


		<?php $_from = $this->_tpl_vars['errorMessages']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['message']):
?>
			* <?php echo $this->_tpl_vars['message']; ?>


		<?php endforeach; endif; unset($_from); ?>
			
		<?php if (isset ( $this->_tpl_vars['deactivatedPlugins'] ) && count ( $this->_tpl_vars['deactivatedPlugins'] ) > 0): ?>
			<?php $this->assign('listOfDeactivatedPlugins', implode($this->_tpl_vars['deactivatedPlugins'], ', ')); ?>
			[!] <?php echo ((is_array($_tmp=((is_array($_tmp='CoreUpdater_WeAutomaticallyDeactivatedTheFollowingPlugins')) ? $this->_run_mod_handler('translate', true, $_tmp, $this->_tpl_vars['listOfDeactivatedPlugins']) : smarty_modifier_translate($_tmp, $this->_tpl_vars['listOfDeactivatedPlugins'])))) ? $this->_run_mod_handler('unescape', true, $_tmp) : smarty_modifier_unescape($_tmp)); ?>


		<?php endif; ?>
	<?php endif; ?>
	<?php if (count ( $this->_tpl_vars['errorMessages'] ) > 0 || count ( $this->_tpl_vars['warningMessages'] ) > 0): ?>
		<?php echo ((is_array($_tmp=((is_array($_tmp='CoreUpdater_HelpMessageIntroductionWhenWarning')) ? $this->_run_mod_handler('translate', true, $_tmp) : smarty_modifier_translate($_tmp)))) ? $this->_run_mod_handler('unescape', true, $_tmp) : smarty_modifier_unescape($_tmp)); ?>


		* <?php echo $this->_tpl_vars['helpMessage']; ?>

	<?php else: ?>
		<?php echo ((is_array($_tmp=((is_array($_tmp='CoreUpdater_PiwikHasBeenSuccessfullyUpgraded')) ? $this->_run_mod_handler('translate', true, $_tmp) : smarty_modifier_translate($_tmp)))) ? $this->_run_mod_handler('unescape', true, $_tmp) : smarty_modifier_unescape($_tmp)); ?>


	<?php endif; ?>
<?php endif; ?>
<?php $_block_content = ob_get_contents(); ob_end_clean(); $_block_repeat=false;echo smarty_block_textformat($this->_tag_stack[count($this->_tag_stack)-1][1], $_block_content, $this, $_block_repeat); }  array_pop($this->_tag_stack); ?>