<?php /* Smarty version 2.6.26, created on 2011-07-28 14:44:20
         compiled from GeoIP/templates/index.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('function', 'postEvent', 'GeoIP/templates/index.tpl', 1, false),array('function', 'sparkline', 'GeoIP/templates/index.tpl', 10, false),)), $this); ?>
<?php echo smarty_function_postEvent(array('name' => 'template_headerGeoIPCountry'), $this);?>


<script type="text/javascript" src="plugins/CoreHome/templates/sparkline.js"></script>
<div id="leftcolumn">
<h2>Country (GeoIP)</h2>
<?php echo $this->_tpl_vars['dataTableCountry']; ?>


<h2>Continent (GeoIP)</h2>
<?php echo $this->_tpl_vars['dataTableContinent']; ?>

<div class="sparkline"><?php echo smarty_function_sparkline(array('src' => $this->_tpl_vars['urlSparklineCountries']), $this);?>
 <strong><?php echo $this->_tpl_vars['numberDistinctCountries']; ?>
 </strong> distinct countries</div>
</div>

<div id="rightcolumn">
<h2>Provider</h2>
<?php if (isset ( $this->_tpl_vars['provider'] )): ?>
<?php echo $this->_tpl_vars['provider']; ?>

<?php endif; ?>
</div>
	

<?php echo smarty_function_postEvent(array('name' => 'template_footerGeoIPCountry'), $this);?>