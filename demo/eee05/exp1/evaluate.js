function colorTheOption (answerId,color) {
	var allLabels = document.getElementsByTagName('label');
	for (var i = allLabels.length - 1; i >= 0; i--) {
		if (allLabels[i].getAttribute("for") == answerId)
			allLabels[i].style.background = color;
	};
}

function highlightWrongAnswers(sectionID) {
	var myRed = 'rgba(238, 52, 17, 0.568627)';
	var allOptions = document.getElementById(sectionID).getElementsByTagName('input');
	for (var i = allOptions.length - 1; i >= 0; i--) {
		if(allOptions[i].checked == true)
		{
			var wrongID = allOptions[i].getAttribute('id');
			colorTheOption(wrongID,myRed);
		};
	};
}

function evaluatePreTest() {
	var answersIds = ['ans6000','ans6003','ans6004','ans6035','ans6047']
	var myGreen = 'rgb(149, 224, 89)';
	highlightWrongAnswers('pretest');
	for (var i = answersIds.length - 1; i >= 0; i--) {
		colorTheOption(answersIds[i], myGreen);
	};
	
}

function evaluatePostTest () {
	var answersIds = ['ans6059','ans6062','ans6063','ans6066','ans6070']
	var myGreen = 'rgb(149, 224, 89)';
	highlightWrongAnswers('posttest');
	for (var i = answersIds.length - 1; i >= 0; i--) {
		colorTheOption(answersIds[i], myGreen)
	};
}