<?php
function unescape($string) {
	$result = "";
	for ($i = 0; $i < strlen($string); $i++) {
		$decstr = "";
		for ($p = 0; $p <= 5; $p++) {
			$decstr .= $string[$i+$p];
		} 
		list($decodedstr, $num) = unescapebycharacter($decstr);
		$result .= urldecode($decodedstr);
		$i += $num ;
	}
	return $result;
}

function unescapebycharacter($str) {

	$char = $str;

	if ($char == '%u20AC') { return array("%80", 5); }
	if ($char == '%u201A') { return array("%82", 5); }
	if ($char == '%u0192') { return array("%83", 5); }
	if ($char == '%u201E') { return array("%84", 5); }
	if ($char == '%u2026') { return array("%85", 5); }
	if ($char == '%u2020') { return array("%86", 5); }
	if ($char == '%u2021') { return array("%87", 5); }
	if ($char == '%u02C6') { return array("%88", 5); }
	if ($char == '%u2030') { return array("%89", 5); }
	if ($char == '%u0160') { return array("%8A", 5); }
	if ($char == '%u2039') { return array("%8B", 5); }
	if ($char == '%u0152') { return array("%8C", 5); }
	if ($char == '%u017D') { return array("%8E", 5); }
	if ($char == '%u2018') { return array("%91", 5); }
	if ($char == '%u2019') { return array("%92", 5); }
	if ($char == '%u201C') { return array("%93", 5); }
	if ($char == '%u201D') { return array("%94", 5); }
	if ($char == '%u2022') { return array("%95", 5); }
	if ($char == '%u2013') { return array("%96", 5); }
	if ($char == '%u2014') { return array("%97", 5); }
	if ($char == '%u02DC') { return array("%98", 5); }
	if ($char == '%u2122') { return array("%99", 5); }
	if ($char == '%u0161') { return array("%9A", 5); }
	if ($char == '%u203A') { return array("%9B", 5); }
	if ($char == '%u0153') { return array("%9C", 5); }
	if ($char == '%u017E') { return array("%9E", 5); }
	if ($char == '%u0178') { return array("%9F", 5); }

	$char = substr($str, 0, 3);
	if ($char == "%20") { return array("+", 2); }

	$char = substr($str, 0, 1);

	if ($char == '*') { return array("%2A", 0); }
	if ($char == '+') { return array("%2B", 0); }
	if ($char == '/') { return array("%2F", 0); }
	if ($char == '@') { return array("%40", 0); }

	if ($char == "%") {
		return array(substr($str, 0, 3), 2);
	} else {
		return array($char, 0);
	}
}
