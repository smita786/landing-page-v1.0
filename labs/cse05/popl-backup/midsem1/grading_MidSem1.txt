1. Free and Bound variables  [20 points]
----------------------------------------

For each "INCORRECTLY" identified variable reference - reduce 1 mark.


2. AST Evaluation     [20 points]
---------------------------------

50% for correct AST
	- For partially correct ASTs, assign partial marks depending upon how close is it to the solution

50% for correct evaluation
	- 25% for correctly annotating the environments
	- 25% for correctly annotating the value at the nodes


3. List recursion [10 points]
-----------------------------

- AS SPECIFIED IN THE QUESTION PAPER ITSELF

Signature:  [2 points]
----------

list-merge: __________________________

(define list-merge
 
 (lambda ( __________   __________ )                   [1 point each]
   
   (cond                                               [2 points each]
     
     [(null? ls1) _________________ ]
     
     
     [(null? ls2) _________________ ]
     
     [else
       
       ( _______________


          ( ___________  ls1)


          ( cons

             ( ___________  ls2)

          
             ( _____________________


                ( ___________  ls1)

           
                ( ___________  ls2))))])))



4. Higher Order functions [20 points]
-------------------------------------

- Signature of thw written function (to be checked against the one expected) 		: 6 marks
- Basic logic (to be evaluated depending upon basic and trivial test cases)  		: 8 marks
- Handling of boundary/special conditions (to be verified using typical test cases)	: 6 marks


5. Abstract Data Types   [20 points]
------------------------------------

* A description (3 sentences maximu) in english that
 specifies your representation of rationals.  [2 points]

* A constructor rational that takes two integers, the
  second nonzero, and returns a rational number.   [4 points]
  - 1 mark for signatire
  - 2 marks for definition (in case of any errors - reduce marks depending upon the seriousness)
  - 1 mark for use cases (0.5 for each)


* A type predicate rational? that takes anything and
  returns a boolean if the argument denotes a rational
  number.   [6 points]
  - 1 mark for signatire
  - 4 marks for definition (in case of any errors - reduce marks depending upon the seriousness)
  - 1 mark for use cases (0.5 for each)

* Projectors rational-numerator and rational-denominator
  that take a rational and returns the numerator and
  denominator, respectively.  [4 points]
  - 1 mark for signatire
  - 2 marks for definition (in case of any errors - reduce marks depending upon the seriousness)
  - 1 mark for use cases (0.5 for each)

* A binary predicate rational=? that takes two rational
  numbers and determines whether the two are equal to
  each other mathematically.  [E.g, 1/2 and 2/4 are
  considered equal mathematically].   [4 points]
  - 1 mark for signatire
  - 2 marks for definition (in case of any errors - reduce marks depending upon the seriousness)
  - 1 mark for use cases (0.5 for each)
