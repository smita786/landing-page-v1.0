;;; POPL 2010 Midterm 2
;;; ===================


;;; Total Time: 90 minutes
;;;
;;; Total Points: 90: Any score of more than 90 on the exam
;;; will be rounded down to 90.


;;; Question 1: The CALCULATOR language
;;; -------------------------------
;;; Points: 30.  5 bonus points if you use 'match'.
;;;
;;; Implement the language CALCULATOR which has the
;;; following concrete syntax:
;;;
;;; Concrete syntax: 
;;; ----------------
;;; <exp>::= <number> 
;;;          | ('+' <exp> <exp>)
;;; 	     | ('-' <exp> <exp>)  
;;;
;;; The calculator should be implemented as the function
;;; eval-exp, which should take an expression as its
;;; argument and return a number as the result of
;;; evaluation.  eval-exp should reject incorrect concrete
;;; syntax.


;;; Question 2: Design of a mini language
;;; -------------------------------------
;;;
;;; Points 60.  5 bonus points if you use match.
;;;
;;; Your goal is to provide a reasonable design and
;;; implemenation (in Scheme) of a mini language whose
;;; syntax and semantics you need to reconstruct based on
;;; the following *examples*.
;;;
;;; In each of these, "go" is the name of the top-level scheme
;;; function that takes an instance e of the concrete syntax and
;;; returns a value obtained by evaluating e.
;;;
;;; (go 5) => error
;;; (go 'a) => error
;;; (go #t) => error
;;; (go "hello") => error
;;;
;;; (go '(& ())) => ()
;;; (go '(& (a b))) => (a b)
;;; (go 'zzz) => ()
;;; (go '(* 3 (& (a b c)))) => (a b c a b c a b c)
;;; (go '(* 1 (& (a b c)))) => (a b c)
;;; (go '(* 0 (& (a b c)))) => ()
;;; (go '(* -1 (& (a b c)))) => (c b a)
;;; (go '(* -3 (& (a b c)))) => (c b a c b a c b a)
;;; (go '(+ (& (a 1 2)) (& (e f g)))) => (a 1 2 e f g)
;;; (go '(+ zzz (& (a b c)))) => (a b c)
;;;
;;; The right hand side of the => indicates the values.
;;;
;;; 'Reasonable' means you can not make trivial assumptions
;;; like saying, for example, that the language has only the
;;; programs listed above.  The examples below must
;;; generalise in a natural way in your object language.
;;; Likewise, please use judgement to reasonably restrict
;;; your language; otherwise you might take too long to
;;; design the solution!
;;;
;;; Your design should clearly identify the following
;;; components:
;;;
;;; 1. Specification of concrete syntax
;;; 2. Abstract syntax tree specification and implementation
;;; 3. Parser
;;; 4. Semantic domains:  expressible and denotable values
;;; 5. Environments
;;; 6. Evaluator
;;; 7. A definition of go
;;;
;;; If you think that your implementation does not need a
;;; particular component from the above, justify.
;;;
;;; You may assume the existence of an implementation of
;;; environments (so you don't need to write it).

;;; You may assume the existence of scheme functions append
;;; and reverse that operate on lists.  Their meaning should
;;; be obvious. However, define and implement any additional
;;; function(s) that you use.



