#lang scheme

;;; Begin Solution 1
;;; ================

(provide
  eval-exp
  eval-exp2a
  eval-exp2b)

;;; Method 1:   eval-exp uses match
;;; ================================
(define eval-exp
  (lambda (e)
    (match e
      [(? number? n) n]
      [(list '+ e1 e2) (+ (eval-exp e1) (eval-exp e2))]
      [(list '- e1 e2) (- (eval-exp e1) (eval-exp e2))]
      [_ (error 'eval-exp "incorrect syntax ~a" e)])))


;;; Method 2:  uses abstract syntax trees
;;; =====================================

(require eopl/eopl)

;;; ast datatype
(define-datatype ast ast?
  [number (n number?)]
  [plus (left ast?) (right ast?)]
  [minus (left ast?) (right ast?)])

;;; expressible value
(define expressible-value? number?)

;;; eval-ast : ast? -> expressible-value?
(define eval-ast
  (lambda (a)
    (cases ast a
      [number (n) n]
      [plus (left right) (+ (eval-ast left) (eval-ast right))]
      [minus (left right) (- (eval-ast left) (eval-ast right))])))


;;; Method 2a: parser uses match
;;; ============================
;;; parse2a : <exp> -> ast?

(define parse2a
  (lambda (e)
    (match e
      [(? number? n) (number n)]
      [(list '+ e1 e2) (plus (parse2a e1) (parse2a e2))]
      [(list '- e1 e2) (minus (parse2a e1) (parse2a e2))]
      [_ (error 'parse2a "incorrect syntax ~a" e)])))

(define eval-exp2a
  (lambda (e)
    (eval-ast (parse2a e))))


;;; Method 2b: parser without match
;;; ===============================
;;; parse2b : <exp> -> ast?


(define parse2b
  (lambda (e)
    (cond
     [(number? e) (number e)]
     [(and (list? e)
        (= (length e) 3)
        (memq (first e) '(+ -)))
      (let ([c (if (eq? (first e) '+) plus minus)]
            [left (second e)]
            [right (third e)])
        (c (parse2b left) (parse2b right)))])))


(define eval-exp2b
  (lambda (e)
    (eval-ast (parse2b e))))



;;; End Solution 1
;;; ==============



(require rackunit)

(check-equal? (eval-exp 4) 4)
(check-equal? (eval-exp '(+ 3 4)) 7)
(check-equal? (eval-exp '(+ (- 2 8) 9)) 3)

(check-exn exn? (lambda () (eval-exp "hello")))
(check-exn exn? (lambda () (eval-exp '(* 2 3))))


(check-equal? (eval-exp2a 4) 4)
(check-equal? (eval-exp2a '(+ 3 4)) 7)
(check-equal? (eval-exp2a '(+ (- 2 8) 9)) 3)

(check-exn exn? (lambda () (eval-exp2a "hello")))
(check-exn exn? (lambda () (eval-exp2a '(* 2 3))))


(check-equal? (eval-exp2b 4) 4)
(check-equal? (eval-exp2b '(+ 3 4)) 7)
(check-equal? (eval-exp2b '(+ (- 2 8) 9)) 3)

(check-exn exn? (lambda () (eval-exp2b "hello")))
(check-exn exn? (lambda () (eval-exp2b '(* 2 3))))

