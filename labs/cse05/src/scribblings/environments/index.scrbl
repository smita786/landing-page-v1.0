#lang scribble/doc

@(require scribble/manual
          scribble/bnf
          scribblings/scribble/utils
	  scribblings/guide/modfile
          (for-label scriblib/figure))

@title[#:tag "environments" #:style '(toc)]{ENVIRONMENT: Abstract Datatype for Lexical Environments}

@local-table-of-contents[#:style 'immediate-only]

  @include-section["objective.scrbl"]
  @include-section["theory.scrbl"]
  @include-section["procedure.scrbl"]
  @include-section["programming.scrbl"]
  @include-section["quiz.scrbl"]
  @include-section["further-reading.scrbl"]
