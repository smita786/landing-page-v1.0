#lang scribble/doc

@(require scribble/manual
          scribble/bnf
          scribblings/scribble/utils
          (for-label scriblib/figure))

@title[#:tag "objective-2"]{Objective}


The objectives of this experiment are the following:

@itemlist[#:style 'ordered
	@item{Learning about higher order functions and about two ways of writing them in scheme.}
	@item{Learn how to define other functions using higher order functions, particularly list-map and list-reduce.}
]

At the end of this experiment, the student is expected to
know how to write higher order functions and use them to construct 
other useful functions.

