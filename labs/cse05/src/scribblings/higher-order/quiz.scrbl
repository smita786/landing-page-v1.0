#lang scribble/doc

@(require scribble/manual
		scribble/bnf
		scribblings/scribble/utils
		(for-label scriblib/figure))

@title[#:tag "quiz-3"]{Quiz}

@itemlist[#:style 'ordered
@item{Write the definition of @emph{list-sum} using @emph{reduce}.}
@item{Re-write the definition of @emph{list-map} in a better way, using a local @emph{letrec}}
]
