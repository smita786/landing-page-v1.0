#lang scribble/doc

@(require scribble/manual
          scribble/bnf
          scribblings/scribble/utils
          (for-label scriblib/figure))

@title[#:tag "further-reading-1"]{Further-reading}

Esentials Of Programming Languages : 
@(let [(url "http://www.cs.indiana.edu/eopl/")]
                (link url url))



