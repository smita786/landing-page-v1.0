#lang scribble/doc

@(require scribble/manual
          scribble/bnf
          scribblings/scribble/utils
	  scribblings/guide/modfile
          (for-label scriblib/figure))

@title[#:tag "lexical" #:style '(toc)]{LEXICAL: A Language with Block Structure}

@local-table-of-contents[]

  @include-section["objective.scrbl"]
  @include-section["theory.scrbl"]
  @include-section["procedure.scrbl"]
  @include-section["programming.scrbl"]
  @include-section["quiz.scrbl"]
  @include-section["further-reading.scrbl"]
