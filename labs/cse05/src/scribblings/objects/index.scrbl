#lang scribble/doc

@(require scribble/manual
          scribble/bnf
          scribblings/scribble/utils
	  scribblings/guide/modfile
          (for-label scriblib/figure))

@title[#:tag "objects" #:style '(toc)]{OBJECTS: A Language for Object Oriented Programming}

@local-table-of-contents[]

  @include-section["objective.scrbl"]
  @include-section["theory.scrbl"]
  @include-section["procedure.scrbl"]
  @include-section["programming.scrbl"]
  @include-section["quiz.scrbl"]
  @include-section["further-reading.scrbl"]
