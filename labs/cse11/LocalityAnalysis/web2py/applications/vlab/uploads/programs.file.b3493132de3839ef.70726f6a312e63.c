/* name - shrenik lad
   roll no- 200901097
   batch - T3 */

/* assumptions:-
   commnans such as vim etc (which use the terminal) should not be run in background
   during "pid all", pid of current a.out will not be displayed.
*/

#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include<sys/types.h>
#include<unistd.h>
#include<errno.h>
#include<signal.h>
#include<sys/wait.h>

typedef struct process{
	char name[100];
	int pid;
	int flag;
}process;

process proc[1000];
int ctr;
char * get_ps1(char * cwd,char * home)
{
	int i=0,j=0,k=0;
	char* ans=(char*)malloc(100*sizeof(char));
	while(home[i]==cwd[i] && i<strlen(home))
		i++;
	if(i==strlen(home))
	{
		ans[k++]='~';
		for(j=i;j<strlen(cwd);j++)
			ans[k++]=cwd[j];
		ans[k]='\0';
	}
	else
	{
		for(j=0;j<strlen(cwd);j++)
			ans[k++]=cwd[j];
		ans[k]='\0';
	}
	return ans;
}
	
int check_bg(char * cmd)
{
	char *parsed;
	char prev[50];
	parsed=strtok(cmd," \t\n");
	while(parsed!=NULL)
	{
		strcpy(prev,parsed);
		parsed=strtok(NULL," \t\n");
	}
	if(prev[strlen(prev)-1]=='&')
		return 1;
	else 
		return 0;
}
	
char * give_new_cmd(char * cmd)
{
	char * new_cmd=(char*)calloc(100,sizeof(char));
	int i;
	for(i=0;i<strlen(cmd);i++)
	{
		if(cmd[i]!='&')
			new_cmd[i]=cmd[i];
	}
	new_cmd[i]='\0';
	return new_cmd;
}

void handler_bg(int sig)
{
	pid_t pid; int i;
	pid=waitpid(-1,NULL,0);
	printf("\npid %d exited\n",pid);
	for(i=0;i<ctr;i++)
	{
		if(proc[i].pid==pid)
		{
			proc[i].flag=1;
			break;
		}
	}
}

void handler_fg(int sig)
{
	proc[ctr].flag=1;
	ctr++;
}

int main()
{
	signal(SIGCHLD,handler_bg);
	ctr=0;
	char * user;
	char host[50],cwd[50],ps1[100],info_copy[100],info[100];
	char *cmd1=(char*)calloc(100,sizeof(char));
	char * new_cmd;
	char **args=(char**)calloc(50,sizeof(char*));
	char cmd_cpy1[100],cmd_cpy2[100],cmd_cpy3[100];
	char *home;
	user=getenv("USER");
	gethostname(host,50);
	home=getenv("HOME");
	getcwd(cwd,50);
	char * parsed;
	int ret,i,bg;
	for(i=0;i<50;i++)
		args[i]=(char*)calloc(50,sizeof(char));
	pid_t pid;
	strcpy(info,user);
	strcat(info,"@");
	strcat(info,host);
	strcat(info,":");
	strcpy(ps1,info);
	chdir(cwd);
	strcpy(info_copy,info);
	strcat(info_copy,get_ps1(cwd,home));
	strcpy(ps1,info_copy);
	while(1)
	{
		errno=0;bg=0;
		args=(char**)calloc(50,sizeof(char*));
		for(i=0;i<50;i++)
			args[i]=(char*)calloc(50,sizeof(char));
		strcpy(info_copy,info);
		printf("%s>>",ps1);
		scanf(" %[^\n]",cmd1);
		strcpy(cmd_cpy1,cmd1);
		strcpy(cmd_cpy2,cmd1);
		strcpy(cmd_cpy3,cmd1);
		if(strcmp(cmd1,"exit")==0)
		{
			for(i=0;i<ctr;i++)
			{
				if(!proc[i].flag)
					ret=kill(proc[i].pid,9);
			}
			break;
		}
		parsed=strtok(cmd1," \t\n");
		if(strcmp(parsed,"cd")==0)
		{
			parsed=strtok(NULL," \t\n");
			if(parsed==NULL)
			{
				ret=chdir(home);
				if(ret)
					perror(NULL);
				else
				{
					strcat(info_copy,"~");
					strcpy(ps1,info_copy);
				}
			}
			else if(parsed!=NULL)
				{
				ret=chdir(parsed);
				if(!ret)
				{
					getcwd(cwd,50);
					strcat(info_copy,get_ps1(cwd,home));
					strcpy(ps1,info_copy);
				}
				else
					perror(NULL);
					
			}
		}
		else if(strcmp(parsed,"pid")==0)
		{
			parsed=strtok(NULL," \t\n");
			if(parsed==NULL)
			{
				pid=getpid();
				printf("command name:./a.out process id: %d\n",pid);
		
			}
			else if(strcmp(parsed,"current")==0)
			{
				for(i=0;i<ctr;i++)
					if(proc[i].flag==0)
						printf("command name : %s pid = %d\n",proc[i].name,proc[i].pid);
			}
			else if(strcmp(parsed,"all")==0)
			{
				for(i=0;i<ctr;i++)
					printf("process : %s, pid : %d \n",proc[i].name,proc[i].pid);
			}
			else
				printf("No such command found\n");
		}
		else
		{
			bg=check_bg(cmd_cpy1);
			strcpy(proc[ctr].name,parsed);
			proc[ctr].flag=0;
			if(bg==1)
			{
				new_cmd=give_new_cmd(cmd_cpy2);
				parsed=strtok(new_cmd," \t\n");
			}
			if(bg==0)
				parsed=strtok(cmd_cpy2," \t\n");
			for(i=0;parsed!=NULL ;i++)
			{
				strcpy(args[i],parsed);
				parsed=strtok(NULL," \t\n");
			}
			args[i]=NULL;
			if (!bg)
				signal(SIGCHLD,handler_fg);
			pid=fork();
			proc[ctr].pid=pid;
			if(pid==0)
			{
				execvp(args[0],args);
				if(errno!=0)
				{
					perror(NULL);
					exit(0);
				}
			}
			else
			{
				if(!bg)
				{
					waitpid(pid,NULL,0);
					signal(SIGCHLD,handler_bg);
				}
				else if(bg)
				{
					signal(SIGCHLD,handler_bg);
					ctr++;
				}
			}

		}
		for(i=0;i<50;i++)
			free(args[i]);
		free(args);
	}
return (0);
}	
