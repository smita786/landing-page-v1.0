/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */


import java.util.ArrayList;


/**
 *
 *
 *
 * @author Abhi
 */
public class CorrespondenceRules
{
////
   public static ArrayList<POint> closetPointRule(ArrayList<POint> scanCurr, ArrayList<POint> scanRef)
   {
      ArrayList<POint> corres = new ArrayList<POint>();

      double dis, min = Double.MAX_VALUE;
      POint p = new POint();
      for(int i = 0; i < scanCurr.size(); i++)
      {
         min = Double.MAX_VALUE;
         for(int j = 0; j < scanRef.size(); j++)
         {
            dis = scanCurr.get(i).dist(scanRef.get(j));
            if(dis < min)
            {
               min = dis;
               p = new POint(scanRef.get(j));
            }

         }
         corres.add(p);
      }
      return corres;
   }

   public static ArrayList<POint> matchingRangePointRule(ArrayList<POint> scanCurr, ArrayList<POint> scanRef)
   {
      ArrayList<POint> corres = new ArrayList<POint>();
      double dis, min = Double.MAX_VALUE;
      double theta, ang, angl, angh;
      POint p = new POint();
      for(int i = 0; i < scanCurr.size(); i++)
      {
         min = Double.MAX_VALUE;
         theta = scanCurr.get(i).polar();
         for(int j = 0; j < scanRef.size(); j++)
         {
            dis = scanCurr.get(i).dist(new POint(0, 0)) - scanRef.get(j).dist(new POint(0, 0));
            dis = Math.abs(dis);
            ang = scanRef.get(j).polar();
            angl = theta - 10.0;
            if(angl < 0)
            {
               angl = 360.0 - angl;
            }
            angh = theta + 10;
            if(angh >= 360)
            {
               angh = angh - 360.0;
            }
            if(dis < min && ang >= angl && ang <= angh)
            {
               min = dis;
               p = new POint(scanRef.get(j));
            }

         }
         corres.add(p);
      }
      return corres;
   }

  
}
