/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */



/**
 *
 * @author Abhi
 */
import java.util.ArrayList;
import java.util.Iterator;
public class Grid
{

   private int m;
   private int n;
   private int nObst;
   private ArrayList<Rect> obstacles;
   //first in this list is always border
   /* Grid(){
   obstacles=new ArrayList<Rect>();
   nObst=0;
   }*/

   public Grid(int width, int height) {
      m = width;
      n = height;
      obstacles = new ArrayList<Rect>();
      obstacles.add(new Rect(new POint(0, 0), getM(), getN()));
      nObst++;
   }

   public void addObstacle(Rect rect) {
      getObstacles().add(rect);
      setnObst(getnObst() + 1);
   }
   /**
    *
    * @param p
    * @return
    */
   public boolean checkInsideObstacles(POint p)
   {
      for(int i = 1; i < obstacles.size(); i++)
      {
         boolean flag=obstacles.get(i).insideRectangle(p);

         if(flag)
         {

            return true;
         }

      }
      return false;
   }
public boolean checkInsideMap(POint p)
{
return obstacles.get(0).insideRectangle(p);
}
   public ArrayList<POint> rayTracing(Pose P) {
	   ArrayList<POint> scan = new ArrayList<POint>();
	   double ang;
	   POint temp, temp1;
	   for (ang = 0; ang <360; ang++) {
		   temp = rayTracingAngle(P.getP(), (ang+P.getAngle())%360);
		   //temp1=temp.invTransform(P.getP(),P.getAngle());
		   scan.add(temp);
	   }

	   for (int i = 0; i < scan.size(); i++) {
		   POint p = scan.get(i).invTransform(P.getP(), P.getAngle());
		   //     p.displayln();
		   scan.set(i, p);
	   }
	   return scan;
   }
public boolean checkValidPoint(POint p)
{
return checkInsideMap(p) && !checkInsideObstacles(p);
}
   public POint rayTracingAngle(POint P, double angle) {
	   POint tempP, minP = null;
	   double tempD, minD = Double.MAX_VALUE;
	   for (int i = 0; i < obstacles.size(); i++) {
		   Rect rectangle = obstacles.get(i);
         tempP = rectangle.interRay(new Ray(P, angle));
         if (tempP != null) {
            tempD = P.dist(tempP);
            if (tempD < minD) {
               minD = tempD;
               minP = tempP;
            }
         }
      }
      return minP;

   }

   /**
    * @return the m
    */
   public int getM() {
      return m;
   }

   /**
    * @param m the m to set
    */
   public void setM(int m) {
      this.m = m;
   }

   /**
    * @return the n
    */
   public int getN() {
      return n;
   }

   /**
    * @param n the n to set
    */
   public void setN(int n) {
      this.n = n;
   }

   /**
    * @return the nObst
    */
   public int getnObst() {
      return nObst;
   }

   /**
    * @param nObst the nObst to set
    */
   public void setnObst(int nObst) {
      this.nObst = nObst;
   }

   /**
    * @return the obstacles
    */
   public ArrayList<Rect> getObstacles() {
      return obstacles;
   }

   /**
    * @param obstacles the obstacles to set
    */
   public void setObstacles(ArrayList<Rect> obstacles) {
      this.obstacles = obstacles;
   }
}
