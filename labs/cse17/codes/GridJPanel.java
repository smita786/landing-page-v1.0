/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Abhi
 */
import java.util.ArrayList;
import java.awt.Graphics;
import java.awt.Color;
import java.awt.Font;

public class GridJPanel extends javax.swing.JPanel
{
   private int WIDTH;
   private int HEIGHT;
   int flag;
   ArrayList<POint> scanCurrOld;
   ArrayList<POint> scanReal;
   ArrayList<POint> scanCurrCorrected;
   double scale = 3;

   public GridJPanel(int WIDTH, int HEIGHT)
   {
      super();
        setWIDTH(WIDTH);
      setHEIGHT(HEIGHT);
      flag = 0;
      scanCurrOld = new ArrayList<POint>();
      scanCurrCorrected = new ArrayList<POint>();
      scanReal = new ArrayList<POint>();
   }

   void setScanCurrOld(ArrayList<POint> scan)
   {
      scanCurrOld = new ArrayList<POint>(scan);
   }

   void setScanCurrCorrected(ArrayList<POint> scan)
   {
      scanCurrCorrected = new ArrayList<POint>(scan);
   }

   void setScanReal(ArrayList<POint> scan)
   {
      scanReal = new ArrayList<POint>(scan);
   }

   void reset()
   {
  /*    scanCurrOld = new ArrayList<POint>();
      scanCurrCorrected = new ArrayList<POint>();
      scanReal = new ArrayList<POint>();
   */
scanCurrOld.clear();
scanCurrCorrected.clear();
scanReal.clear();
      repaint();
   
   }


   protected void paintComponent(Graphics g)
   {

	   super.paintComponent(g);
      g.setColor(Color.DARK_GRAY);
      g.drawLine(0, HEIGHT/2, HEIGHT, HEIGHT/2);
      g.drawLine(HEIGHT/2, 0, HEIGHT/2, HEIGHT);
      g.setFont(new Font(g.getFont().getName(),Font.BOLD, g.getFont().getSize()));
      g.drawString("0\u00B0", 280, 140);
      g.drawString("90\u00B0", 130, 20);
      g.drawString("180\u00B0", 10, 170);
      g.drawString("270\u00B0", 160, 290);
	     g.setColor(Color.BLACK);
	   for(int i = 0; i < scanReal.size(); i++)
	   {
		   POint p = scanReal.get(i);
		   g.fillOval((int) (p.getX() / scale) - 1 + HEIGHT/2, HEIGHT - (int) (p.getY() / scale) - 1 - HEIGHT/2, 2, 2);
	   }
        
      g.setColor(Color.RED);
	   for(int i = 0; i < scanCurrOld.size(); i++)
	   {
		   POint p = scanCurrOld.get(i);
		   g.fillOval((int) (p.getX() / scale) - 1 + HEIGHT/2, HEIGHT - (int) (p.getY() / scale) - 1 - HEIGHT/2, 2, 2);
	   }

	   g.setColor(Color.GREEN);
	   for(int i = 0; i < scanCurrCorrected.size(); i++)
	   {
		   POint p = scanCurrCorrected.get(i);
		   g.fillOval((int) (p.getX() / scale) - 1 + HEIGHT/2, HEIGHT - (int) (p.getY() / scale) - 1 - HEIGHT/2, 2, 2);
	   }
	   //      *****Drawing ScanCurrCorrectez
	 
   }

   /**
    * @return the WIDTH
    */
   public int getWIDTH()
   {
      return WIDTH;
   }

   /**
    * @param WIDTH the WIDTH to set
    */
   public void setWIDTH(int WIDTH)
   {
      this.WIDTH = WIDTH;
   }

   /**
    * @return the HEIGHT
    */
   public int getHEIGHT()
   {
      return HEIGHT;
   }

   /**
    * @param HEIGHT the HEIGHT to set
    */
   public void setHEIGHT(int HEIGHT)
   {
      this.HEIGHT = HEIGHT;
   }
}
