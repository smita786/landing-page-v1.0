/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */



import javax.swing.SwingUtilities;

public class NewApplet extends javax.swing.JApplet
{
    //Called when this applet is loaded into the browser.

    @Override
    public void init()
    {
        //Execute a job on the event-dispatching thread; creating this applet's GUI.
        try {
            SwingUtilities.invokeAndWait(new Runnable() {
                public void run() {
                    createGUI();
                }
            });
        }
        catch (Exception e)
        {
           // System.err.println("createGUI didn't complete successfully");
        }
    }

    /**
     * Create the GUI. For thread safety, this method should
     * be invoked from the event-dispatching thread.
     */
    private void createGUI()
    {

        MainJPanel newContentPane = new MainJPanel(400,400);

        newContentPane.setOpaque(true);
        setContentPane(newContentPane);
    }






    // called when run as an application
    public static void main(String[] args)    {
        //Schedule a job for the event-dispatching thread:
        //creating and showing this application's GUI.
        javax.swing.SwingUtilities.invokeLater(new Runnable() {
            public void run() {
                createAndShowGUI();
            }
        });
    }






      private static void createAndShowGUI()
   {
        //Create and set up the window.
        javax.swing.JFrame frame = new javax.swing.JFrame("Scan Matching");
        frame.setDefaultCloseOperation(javax.swing.JFrame.EXIT_ON_CLOSE);

        //Create and set up the content pane.
        MainJPanel newContentPane = new MainJPanel(400,400);
        newContentPane.setOpaque(true); //content panes must be opaque
        frame.setContentPane(newContentPane);

        //Display the window.
        frame.pack();
        frame.setVisible(true);

//System.out.println(frame.getSize());
    }







}
