/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Abhi
 */
public class POint
{

   private double x;
   private double y;

   /**
    *
    * @param i
    * @param j
    */
   public POint(double i, double j)
   {
      x = i;
      y = j;
   }

   /**
    *
    * @param i
    * @param j
    */
   public POint(int i, int j)
   {
      x = i;
      y = j;
   }

   /**
    *
    */
   public POint(POint P)
   {
      setX(P.getX());
      setY(P.getY());
   }

   public POint()
   {
      x = 0.0;
      y = 0.0;
   }

   void setPolar(double range, int angle)
   {
      setX(range * Math.cos(Math.toRadians(angle)));
      setY(range * Math.sin(Math.toRadians(angle)));

   }

   public double polar()
   {
      double angle = 0.0;
      if(getX() > 0 && getY() > 0)
      {
         angle = Math.atan(getY() / getX());
         angle = Math.toDegrees(angle);
      }
      if(getX() > 0 && getY() < 0)
      {
         angle = Math.atan(getY() / getX());
         angle = Math.toDegrees(angle);
         angle = 360.0 + angle;
      }
      if(getX() < 0 && getY() < 0)
      {
         angle = Math.atan(getY() / getX());
         angle = Math.toDegrees(angle);
         angle = 180.0 + angle;
      }
      if(getX() < 0 && getY() > 0)
      {
         angle = Math.atan(getY() / getX());
         angle = Math.toDegrees(angle);
         angle = 180.0 - angle;
      }
      if(getX() == 0)
      {
         if(getY() > 0)
         {
            angle = 90.0;
         } else
         {
            angle = 270.0;
         }
      }
      if(getY() == 0)
      {
         if(getX() > 0)
         {
            angle = 0.0;
         } else
         {
            angle = 180.0;
         }
      }
      return angle;
   }

   //complete
   public POint transform(POint A, double angle)
   {
      POint pNew = new POint();
      pNew.setX(Math.cos(Math.toRadians(angle)) * getX() - Math.sin(Math.toRadians(angle)) * getY());
      pNew.setY(Math.sin(Math.toRadians(angle)) * getX() + Math.cos(Math.toRadians(angle)) * getY());
      pNew.setX(pNew.getX() + A.getX());
      pNew.setY(pNew.getY() + A.getY());
      return pNew;

   }

   public void applyTransfrom(POint A, double angle)
   {
      POint temp = transform(A, angle);
      setX(temp.getX());
      setY(temp.getY());
   }

   public void applyInvTransfrom(POint A, double angle)
   {
      POint temp = invTransform(A, angle);
      setX(temp.getX());
      setY(temp.getY());
   }

   public POint invTransform(POint A, double angle)
   {
      POint pNew = new POint();
      double x1, y1;
      x1 = getX() - A.getX();
      y1 = getY() - A.getY();

      pNew.setX(Math.cos(Math.toRadians(angle)) * x1 + Math.sin(Math.toRadians(angle)) * y1);
      pNew.setY(-1 * Math.sin(Math.toRadians(angle)) * x1 + Math.cos(Math.toRadians(angle)) * y1);
      return pNew;
   }

   /**
    *
    * @param A
    * @return distance to POint A
    */
   public double dist(POint A)
   {
      return Math.sqrt(Math.pow((getX() - A.getX()), 2) + Math.pow((getY() - A.getY()), 2));
   }

   /**
    * @return true if NULL else false
    */
   boolean checkNUll()
   {
      if(getX() == 0.0 && getY() == 0.0)
      {
         return true;
      } else
      {
         return false;
      }
   }

   /**
    * @return the x
    */
   public double getX()
   {
      return x;
   }

   /**
    * @param x the x to set
    */
   void setX(double x)
   {
      this.x = x;
   }

   /**
    * @return the y
    */
   public double getY()
   {
      return y;
   }

   /**
    * @param y the y to set
    */
   void setY(double y)
   {
      this.y = y;
   }

   public void display()
   {
      System.out.format("(%.2f,%.2f)", getX(), getY());
//      System.out.print("(");
//      System.out.print(getX());
//      System.out.print(",");
//      System.out.print(getY());
//      System.out.print(")");
   }

   public void displayln()
   {
      System.out.format("(%.2f,%.2f)\n", getX(), getY());
//      System.out.print("(");
//      System.out.print(getX());
//      System.out.print(",");
//      System.out.print(getY());
//      System.out.println(")");
   }
}
