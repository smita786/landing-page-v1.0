/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */


/**
 *
 * @author Abhi
 */
public class Ray
{

   private double a;
   private double b;
   private double c;
   private double polarAngle;
   private POint originPoint;

   public Ray(Ray r)
   {
      setA(r.getA());
      setB(r.getB());
      setC(r.getC());
      setOrigin(r.getOrigin());
      setPolarAngle(r.getPolarAngle());
   }

   /**
    *
    * @param B
    * @param ang
    */
   public Ray(POint B, double ang)
   {

      polarAngle = ang;
      originPoint = B;
      if(ang >= 180.0)
      {
         ang -= 180.0;
      }
      if(ang != 90.0)
      {
         double m = Math.tan(Math.toRadians(ang));
         a = m;
         b = -1;
         c = (-1) * m * B.getX() + B.getY();
      } else if(ang == 90.0)
      {
         a = 1;
         b = 0;
         c = (-1) * B.getX();
      }
   }

   public Ray(POint A, POint B)
   {
      if(A.getX() == B.getX())
      {
         setA(1.0);
         setB(0.0);
         setC(-1.0 * A.getX());
      } else if(A.getY() == B.getY())
      {
         setA(0.0);
         setB(1.0);
         setC(-1.0 * A.getY());
      } else
      {
         double m = (A.getY() - B.getY()) / (A.getX() - B.getX());
         setA(m);
         setB(-1.0);
         setC(B.getY() - m * B.getX());
      }

   }

   double val(POint A)
   {
      return getA() * A.getX() + getB() * A.getY() + getC();
   }

   boolean valBool(POint A)
   {
      if(getA() * A.getX() + getB() * A.getY() + getC() > 0)
      {
         return true;
      } else
      {
         return false;
      }
   }

   public POint interLineSegment(POint A, POint B)
   {
      double valA, valB;
      POint temp;
      valA = val(A);
      valB = val(B);
      //    System.out.println(valA);
      //     System.out.println(valB);
      if(valA > 0 && valB > 0)
      {
         return null;
      } else if(valA < 0 && valB < 0)
      {
         return null;
      }

      if(A.getY() == B.getY())
      {
         temp = new POint((getB() * A.getY() + getC()) / (-1 * getA()), A.getY());
      } else
      {
         temp = new POint(A.getX(), (getA() * A.getX() + getC()) / (-1 * getB()));
      }

      if((temp.getY() < getOrigin().getY() && getPolarAngle() > 180) || (temp.getY() > getOrigin().getY() && getPolarAngle() < 180))
      {
         return temp;
      } else if((temp.getX() < getOrigin().getX() && getPolarAngle() == 180) || (temp.getX() > getOrigin().getX() && getPolarAngle() == 0))
      {

         return temp;
      } else
      {
         return null;
      }

   }

   /**
    * @return the a
    */
   public double getA()
   {
      return a;
   }

   /**
    * @param a the a to set
    */
   void setA(double a)
   {
      this.a = a;
   }

   /**
    * @return the b
    */
   public double getB()
   {
      return b;
   }

   /**
    * @param b the b to set
    */
   void setB(double b)
   {
      this.b = b;
   }

   /**
    * @return the c
    */
   public double getC()
   {
      return c;
   }

   /**
    * @param c the c to set
    */
   void setC(double c)
   {
      this.c = c;
   }

   /**
    * @return the polarAngle
    */
   public double getPolarAngle()
   {
      return polarAngle;
   }

   /**
    * @param polarAngle the polarAngle to set
    */
   void setPolarAngle(double polarAngle)
   {
      this.polarAngle = polarAngle;
   }

   /**
    * @return the originPoint
    */
   public POint getOrigin()
   {
      return originPoint;
   }

   /**
    * @param originPoint the originPoint to set
    */
   void setOrigin(POint origin)
   {
      this.originPoint = new POint(origin);
   }

   public void displayln()
   {
      System.out.print("L");
      System.out.print(getA());
      System.out.print(",");
      System.out.print(getB());
      System.out.print(",");
      System.out.print(getC());
    //  originPoint.display();
//      System.out.print(",");
//      System.out.print(getPolarAngle());
      System.out.println("L");
   }
}