/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */


import java.util.ArrayList;

/**
 *
 * @author Abhi
 */
public class Rect
{

   private POint cornerA;
   private POint cornerB;
   private POint cornerC;
   private POint cornerD;
   private ArrayList<POint> vertices;
   private double width;
   private double height;
   //bottom left corner,width along x axis,height along y axis
   //in anti clock wise order A,B,C,D

   public Rect(POint P, double width, double height)
   {
      cornerA = new POint(P);
      cornerB = new POint(P.getX() + width, P.getY());
      cornerC = new POint(P.getX() + width, P.getY() + height);
      cornerD = new POint(P.getX(), P.getY() + height);
      setWidth(width);
      setHeight(height);
      vertices = new ArrayList<POint>();
      vertices.add(cornerA);
      vertices.add(cornerB);
      vertices.add(cornerC);
      vertices.add(cornerD);
      /*
      cornerA=P;
      cornerB.setY(P.getY());
      cornerD.setX(P.getX());
      cornerB.setX(P.getX() + width - 1);
      cornerD.setY(P.getY() + height + 1);
      cornerC.setX(P.getX() + width - 1);
      cornerC.setY(P.getY() + height + 1);*/
   }

   /**
    *
    * @param P
    * @return
    */
   public boolean insideRectangle(POint P)
   {
      if(P.getX() >=getCornerA().getX() && P.getX() <=getCornerB().getX() && P.getY()>=getCornerA().getY() && P.getY()<=getCornerD().getY())
         return true;
      else
         return false;
//      boolean temp;
//      Ray t=(new Ray(vertices.get(0), vertices.get(1)));
//      temp = t.valBool(P);
//       System.out.print("0+ ");
//          t.displayln();
//          System.out.println(t.valBool(P));
//      for(int i = 1; i < 4; i++)
//      {
//           Ray t1=new Ray(vertices.get(i % 4), vertices.get((i + 1) % 4));
//          System.out.print(i+ " ");
//          t1.displayln();
//          System.out.println(t1.valBool(P));
//         if(temp != t1.valBool(P))
//         {
//            return false;
//         }
//      }

   }

   public POint interRay(Ray l)
   {
      POint temp, curr = null;
      double d, min = Double.MAX_VALUE;

      temp = l.interLineSegment(getCornerA(), getCornerB());
      if(temp != null)
      {
         d = temp.dist(l.getOrigin());
         if(d < min)
         {
            min = d;
            curr = new POint(temp);
         }
      }
      temp = l.interLineSegment(getCornerB(), getCornerC());
      if(temp != null)
      {
         d = temp.dist(l.getOrigin());
         if(d < min)
         {
            min = d;
            curr = new POint(temp);
         }
      }
      temp = l.interLineSegment(getCornerC(), getCornerD());
      if(temp != null)
      {
         d = temp.dist(l.getOrigin());
         if(d < min)
         {
            min = d;
            curr = new POint(temp);
         }
      }
      temp = l.interLineSegment(getCornerD(), getCornerA());
      if(temp != null)
      {
         d = temp.dist(l.getOrigin());
         if(d < min)
         {
            min = d;
            curr = new POint(temp);
         }
      }

      return curr;
   }

   /**
    * calculates intersection point
    *
    * @return the distance
    */
   public double interRayDist(Ray l)
   {
      POint temp = interRay(l);
      if(temp != null)
      {
         return temp.dist(l.getOrigin());
      }
      return Double.MAX_VALUE;
   }

   /**
    * @return the cornerA
    */
   public POint getCornerA()
   {
      return cornerA;
   }

   /**
    * Set corner A field
    *
    * @param cornerA the cornerA to set
    */
   void setCornerA(POint cornerA)
   {
      this.cornerA = new POint(cornerA);
   }

   /**
    * @return the cornerB
    */
   public POint getCornerB()
   {
      return cornerB;
   }

   /**
    * @param cornerB the cornerB to set
    */
   void setCornerB(POint cornerB)
   {
      this.cornerB = new POint(cornerB);
   }

   /**
    * @return the cornerC
    */
   public POint getCornerC()
   {
      return cornerC;
   }

   /**
    * @param cornerC the cornerC to set
    */
   void setCornerC(POint cornerC)
   {
      this.cornerC = new POint(cornerC);
   }

   /**
    * @return the cornerD
    */
   public POint getCornerD()
   {
      return cornerD;
   }

   void setCornerD(POint cornerD)
   {
      /**
       * @param cornerD the cornerD to set
       */
      this.cornerD = new POint(cornerD);
   }

   /**
    * @return the width
    */
   public double getWidth()
   {
      return width;
   }

   /**
    * @param width the width to set
    */
   public void setWidth(double width)
   {
      this.width = width;
   }

   /**
    * @return the height
    */
   public double getHeight()
   {
      return height;
   }

   /**
    * @param height the height to set
    */
   public void setHeight(double height)
   {
      this.height = height;
   }

   public void display()
   {
      System.out.print("R");
      this.cornerA.display();
      this.cornerB.display();
      this.cornerC.display();
      this.cornerD.display();
      System.out.println("R");
   }
}