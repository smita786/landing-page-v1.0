/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */



import java.util.ArrayList;
/**
 *
 * @author Abhi
 */
public class Solution {
    /**
     * 
     * @param scanCurr
     * @param corres
     * @return residual pose
     */
    public static Pose leastSquaresSolution(ArrayList<POint> scanCurr,ArrayList<POint> corres){
        Pose poseSol=new Pose();
	int n=scanCurr.size();
	double x=0.0,y=0.0,x1=0.0,y1=0.0;
	double sxx1=0.0,syy1=0.0,sxy1=0.0,syx1=0.0;
    double N=n*1.0;
	//list<point>::iterator it1,it2;

	for(int i=0;i<n;i++)
	{
		x+=scanCurr.get(i).getX();
		y+=scanCurr.get(i).getY();
		x1+=corres.get(i).getX();
		y1+=corres.get(i).getY();

	}
	x/=N;
	y/=N;
	x1/=N;
	y1/=N;
	for(int i=0;i<n;i++)
	{
		sxx1+=(scanCurr.get(i).getX()-x)*(corres.get(i).getX()-x1);
		syy1+=(scanCurr.get(i).getY()-y)*(corres.get(i).getY()-y1);
		sxy1+=(scanCurr.get(i).getX()-x)*(corres.get(i).getY()-y1);
		syx1+=(scanCurr.get(i).getY()-y)*(corres.get(i).getX()-x1);

	}
	sxx1/=N;
	syy1/=N;
	sxy1/=N;
	syx1/=N;
	double w,Tx,Ty;
	w=Math.atan((sxy1-syx1)/(sxx1+syy1));
	Tx=x1-(x*Math.cos(w)-y*Math.sin(w));
	Ty=y1-(x*Math.sin(w)+y*Math.cos(w));
	poseSol.setP(new POint(Tx,Ty));
	poseSol.setAngle(Math.toDegrees(w));

	return poseSol;
    }

}
