import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JButton;
import javax.swing.JSlider;
import java.awt.BorderLayout;
import java.awt.GridLayout;
import javax.swing.JLabel;
import javax.swing.JComboBox;
import java.util.Random;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import java.awt.event.MouseAdapter;
import javax.swing.JTextField;
import javax.swing.JApplet;


public class Frame extends JApplet
{
	public JLabel statusBar;	
	public map Map;	
	public buttonPanel b;
	public JPanel right;
	public Frame()
	{
		setLayout(new  BorderLayout());
		b=new buttonPanel();
		statusBar=new JLabel("Click to move the robot in the desired direction");
		add(statusBar, BorderLayout.SOUTH);
		Map=new map();
		Map.addMouseListener( new MouseAdapter()
		{
			public void mouseClicked(MouseEvent event)
			{
				int xPos=event.getX();
				int yPos=event.getY();	
				if(Map.borders.contains(xPos,yPos)==true)
				{
					String stat=String.format("You clicked at [%d,%d]", xPos, yPos);
					Map.moveTo(xPos,yPos);
					statusBar.setText(stat);
					Map.repaint();	
				}
				else
				{
					String warn=String.format("Please Click Inside the Map\n");
					statusBar.setText(warn);
				}
			}
		});
		add(Map,BorderLayout.CENTER);	
		add(b,BorderLayout.NORTH); 		

		b.Start.addActionListener(new ActionListener()
		{
			public void actionPerformed(ActionEvent e)
			{
				if(b.type.getSelectedIndex()==0)
				{
					Map.InitMapLocal(b.mapList.getSelectedIndex());
					Map.isLocal=true;

				}
				else 
				{	
					Map.InitMapGlobal(b.mapList.getSelectedIndex());
					Map.isLocal=false;
				}
			}
		});	
		


		b.range.addChangeListener(new ChangeListener()
		{
			public void stateChanged(ChangeEvent e)
			{
				Map.maxRange=b.range.getValue();
				Map.repaint();
			}
		});

		b.sense.addActionListener(new ActionListener()
		{
			public void actionPerformed(ActionEvent e)
			{
				Map.Sense();
				Map.repaint();
			}
		});

		b.mapList.addItemListener(new ItemListener()
		{
			public void itemStateChanged(ItemEvent event)
			{
				if(event.getStateChange()==ItemEvent.SELECTED)
				{
					if(b.type.getSelectedIndex()==0)
					{
						Map.InitMapGlobal(b.mapList.getSelectedIndex());
						Map.isLocal=false;
					}
					else 
					{
						Map.InitMapLocal(b.mapList.getSelectedIndex());
						Map.isLocal=true;
					}
				}
			}
		});

		b.sigmaRList.addItemListener(new ItemListener()
		{
			public void itemStateChanged(ItemEvent event)
			{
				if(event.getStateChange()==ItemEvent.SELECTED)
				{
					if(b.sigmaRList.getSelectedIndex()==0) {Map.sigmaR=0.0005;}
					else if(b.sigmaTList.getSelectedIndex()==1) {Map.sigmaR=0.005;}
					else {Map.sigmaR=0.05;}
				}
			}	
		});	

		b.sigmaSenseList.addItemListener(new ItemListener()
		{
			public void itemStateChanged(ItemEvent event)
			{
				if(event.getStateChange()==ItemEvent.SELECTED)
				{
					if(b.sigmaSenseList.getSelectedIndex()==0){Map.sensorSigma=0.1;}
					else if(b.sigmaSenseList.getSelectedIndex()==1){Map.sensorSigma=1;}
					else{Map.sensorSigma=5;}
					
				}
			}

		});


		b.sigmaTList.addItemListener(new ItemListener()
		{
			public void itemStateChanged(ItemEvent event)
			{
				if(event.getStateChange()==ItemEvent.SELECTED)
				{
					if(b.sigmaRList.getSelectedIndex()==0){Map.sigmaTheta=0.009;}
					else if(b.sigmaTList.getSelectedIndex()==1){Map.sigmaTheta=0.05;}
					else {Map.sigmaTheta=0.1;}
				}
			}	
		});	

		b.type.addItemListener(new ItemListener()
		{
			public void itemStateChanged(ItemEvent event)
			{
				if(event.getStateChange()==ItemEvent.SELECTED)
				{
					if(b.type.getSelectedIndex()==0){Map.InitMapLocal(b.mapList.getSelectedIndex());
						Map.isLocal=true;					
					}
					else 
					{
						Map.InitMapGlobal(b.mapList.getSelectedIndex());
						Map.isLocal=false;
					}
				}
			}
		});
		setSize(1200,600);
		setVisible(true);
	//	setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}

	public class buttonPanel extends JPanel
	{
		public JButton Start;
		public JSlider range;
		public JComboBox mapList;
		public JButton sense;
		public String[] Maps={"Map 1", "Map 2", "Map 3"};
		public JLabel rangeLabel;			
		public JComboBox type;
		public String[] types={"Local","Global"};
		public JLabel glo;
		public String[] Rs={"0.5", "5", "50"};
		public JComboBox sigmaRList;
		public String[] Ths={"0.5", "5", "50"};
		public JComboBox sigmaTList;
		public JLabel VarR; 
		public JLabel VarT;
		public String[] sensorNoise={"0.1", "1","10"};
		public JComboBox sigmaSenseList;
		public JLabel VarS;
		public buttonPanel()
		{
			Start=new JButton("Start");	
			rangeLabel=new JLabel("      Range:");
			range=new JSlider(JSlider.HORIZONTAL,0,150,50);
			range.setMajorTickSpacing(50);
			range.setMinorTickSpacing(25);	
			range.setPaintTicks(true);	
			glo=new JLabel("   Type:");
			range.setPaintLabels(true);
			sense=new JButton("Sense");
			type=new JComboBox(types);
			mapList=new JComboBox(Maps);
			sigmaSenseList=new JComboBox(sensorNoise);
			sigmaRList=new JComboBox(Rs);
			sigmaTList=new JComboBox(Ths);
			VarR=new JLabel("Sigma R:");
			VarT= new JLabel("Sigma Theta:");
			VarS=new JLabel("Sensor Noise:");
			add(mapList);	
			add(Start);	
			add(sense);
			add(rangeLabel);
			add(range);
			add(glo);
			add(type);	
			add(VarR);
			add(sigmaRList);
			add(VarT);
			add(sigmaTList);
			add(VarS);
			add(sigmaSenseList);
		}
	}
	
//	public static void main(String[] args)
	//{
	//	Frame frame=new Frame();
	//}
}
