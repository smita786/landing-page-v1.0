
public class Velocity {
    
    public double Omega;
    public double V;
    
    public Velocity(double v, double omega)
    {        
        V=v;
        Omega=omega;        
    }
    
    public double getV()
    {
      return V;
    }
    
    public double getOmega()
    {
        return Omega;
    }
}
