<html>

<head>
<script type="text/javascript">
var Ncount=0;
var Noun=new Array();
var Vcount=0;
var Verb=new Array();
var Acount=0;
var Adjective=new Array();
var Dcount=0;
var Determiner=new Array();
var Pcount=0;
var Pronoun=new Array();

function contains(A,word){
	for(i=0;i<A.length;i++){
		if(A[i]==word){
			return true;
		}
	}
	return false;
}

function classify(word){
	document.getElementById(word).disabled=true;
	xmlhttp=new XMLHttpRequest();
	xmlhttp.onreadystatechange=function(){
		if (xmlhttp.readyState==4 && xmlhttp.status==200){
			wordClass=xmlhttp.responseText;
			if(wordClass=="noun"){
				if(contains(Noun,word)){
					return;
				}
				Noun[Ncount]=word;
				Ncount=Ncount+1;
				sentence="";
				for(i=0;i<Ncount;i++){
					sentence=sentence+"<tr><td>"+Noun[i]+"</td></tr>";
				}
				document.getElementById("noun").innerHTML=sentence;
			}
			if(wordClass=="verb"){
				if(contains(Verb,word)){
					return;
				}
				Verb[Vcount]=word;
				Vcount=Vcount+1;
				sentence="";
				for(i=0;i<Vcount;i++){
					sentence=sentence+"<tr><td>"+Verb[i]+"</td></tr>";
				}
				document.getElementById("verb").innerHTML=sentence;
			}
			if(wordClass=="adjective"){
				if(contains(Adjective,word)){
					return;
				}
				Adjective[Acount]=word;
				Acount=Acount+1;
				sentence="";
				for(i=0;i<Acount;i++){
					sentence=sentence+"<tr><td>"+Adjective[i]+"</td></tr>";
				}
				document.getElementById("adjective").innerHTML=sentence;
			}
			if(wordClass=="determiner"){
				if(contains(Determiner,word)){
					return;
				}
				Determiner[Dcount]=word;
				Dcount=Dcount+1;
				sentence="";
				for(i=0;i<Dcount;i++){
					sentence=sentence+"<tr><td>"+Determiner[i]+"</td></tr>";
				}
				document.getElementById("determiner").innerHTML=sentence;
			}
			if(wordClass=="pronoun"){
				if(contains(Pronoun,word)){
					return;
				}
				Pronoun[Pcount]=word;
				Pcount=Pcount+1;
				sentence="";
				for(i=0;i<Pcount;i++){
					sentence=sentence+"<tr><td>"+Pronoun[i]+"</td></tr>";
				}
				document.getElementById("pronoun").innerHTML=sentence;
			}
			
		}
	}
	xmlhttp.open("GET","getClass.php?w="+word+"&Q=2",true);
	xmlhttp.send();
//	wordClass=classretriever(word)
}

function setvalue(data){
//	alert(data);
	if(data=="null"){
		document.getElementById("sent").innerHTML="Select a sentence !!";
		document.getElementById("classes").innerHTML="";
		return;
	}	
	
chunks=data.split(" ");
sentence="Click on a word to know its POS<br><br>";
for(i=0;i<chunks.length;i++){
	sentence=sentence+"<input type='button' id='"+chunks[i]+"'  onclick='classify(\""+chunks[i]+"\");'  class='mybutton' value='"+chunks[i] + "'>";
}
sentence=sentence+".";
document.getElementById('sent').innerHTML=sentence;
table="<table width='100%' bgcolor=#FFD4A8><tr><th width='20%'>Noun</th><th width='20%'>Pronoun</th><th width='20%'>Verb</th><th width='20%'>Determiner</th><th width='20%'>Adjective</th></tr><tr><td><div id='noun'></div></td><td><div id='pronoun'></div></td><td><div id='verb'></div></td><td><div id='determiner'></div></td><td><div id='adjective'></div></td></tr></table>";
document.getElementById("classes").innerHTML=table;
sentence="";
for(i=0;i<Ncount;i++){
	sentence=sentence+"<tr><td>"+Noun[i]+"</td></tr>";
}
document.getElementById("noun").innerHTML=sentence;
sentence="";
for(i=0;i<Vcount;i++){
	sentence=sentence+"<tr><td>"+Verb[i]+"</td></tr>";
}
document.getElementById("verb").innerHTML=sentence;
sentence="";
for(i=0;i<Pcount;i++){
	sentence=sentence+"<tr><td>"+Pronoun[i]+"</td></tr>";
}
document.getElementById("pronoun").innerHTML=sentence;
sentence="";
for(i=0;i<Acount;i++){
	sentence=sentence+"<tr><td>"+Adjective[i]+"</td></tr>";
}
document.getElementById("adjective").innerHTML=sentence;
sentence="";
for(i=0;i<Dcount;i++){
	sentence=sentence+"<tr><td>"+Determiner[i]+"</td></tr>";
}
document.getElementById("determiner").innerHTML=sentence;
}
</script>

</head>
<style type="text/css">

input.mybutton{
	font-size:16px;
	font-family:Arial,sans-serif;
	height:26px;
       background-color:#779999;
       background-image:url(back03.gif);
       border-style:solid;
       border-color:#DDDDDD;
       border-width:1px;
}
</style>
<body>
<div align="center">
<form>
<select name="sentence">
<option value="null" onclick="setvalue('null');" selected="selected">Select a senetence</option>
<option value=\"s1\" onclick="setvalue('Ram is a good boy');">Ram is a good boy</option>
<option value=\"s2\" onclick="setvalue('He runs fast');"> He runs fast</option>
</select><br><br>
<div id="sent"></div><br>
<div id="class"></div>

</form>
</div>
<!--<table width="100%" bgcolor=#FFD4A8>
<tr>
<th width="20%">Noun</th>
<th width="20%">Pronoun</th>
<th width="20%">Verb</th>
<th width="20%">Determiner</th>
<th width="20%">Adjective</th>
</tr>
<tr>
<td><div id="noun"></div></td>
<td><div id="pronoun"></div></td>
<td><div id="verb"></div></td>
<td><div id="determiner"></div></td>
<td><div id="adjective"></div></td>
</tr>
</table>-->
<div id="classes"></div>
</body>
</html>
