<html>
<body>

Part-of-speech tagging (or just tagging for short) is the process of assigning a part-of-speech or other lexical class marker to each word in a corpus. Tags are also usually applied to punctuation markers; thus tagging for natural language is the same process as tokenization for computer languages, although tags for natural languages are much more ambiguous. As we suggested at the beginning of the chapter, taggers play an increasingly important role in speech recognition, natural language parsing and information retrieval.<br><br>

Each of the traditional part of speech can be made more fine-grained so that can convey additional information. Such a tagset (adapted from the Penn tagset) is shown below.<br><br>
<TABLE BORDER=2  CELLSPACING=2 WIDTH=100% >
<TR>
<TH> POS Tag</TH><TH> Description</TH><TH> Example </TH>
</TR>
<TR>
<TD> CC  </TD><TD> coordinating conjunction  </TD><TD> and </TD>

</TR>
<TR>
<TD> CD  </TD><TD> cardinal number </TD><TD> 1, third </TD>
</TR>
<TR>
<TD> DT  </TD><TD> determiner  </TD><TD> the  </TD>

</TR>
<TR>
<TD> EX  </TD><TD> existential there </TD><TD> <i>there</i> is </TD>
</TR>
<TR>
<TD> FW  </TD><TD> foreign word  </TD><TD> d'hoevre  </TD>

</TR>
<TR>
<TD> IN  </TD><TD> preposition/subordinating conjunction </TD><TD> in, of, like </TD>
</TR>
<TR>
<TD> JJ   </TD><TD> adjective    </TD><TD> green  </TD>

</TR>
<TR>
<TD> JJR   </TD><TD> adjective, comparative    </TD><TD> greener  </TD>
</TR>
<TR>
<TD> JJS   </TD><TD> adjective, superlative    </TD><TD> greenest  </TD>

</TR>
<TR>
<TD> LS  </TD><TD> list marker  </TD><TD> 1)  </TD>
</TR>
<TR>
<TD> MD  </TD><TD> modal </TD><TD> could, will </TD>

</TR>
<TR>
<TD> NN  </TD><TD> noun, singular or mass </TD><TD> table </TD>
</TR>
<TR>
<TD> NNS </TD><TD>  noun plural </TD><TD> tables </TD>

</TR>
<TR>
<TD> NNP </TD><TD>  proper noun, singular  </TD><TD> John      </TD>
</TR>
<TR>
<TD> NNPS </TD><TD> proper noun, plural  </TD><TD> Vikings  </TD>

</TR>
<TR>
<TD> PDT  </TD><TD> predeterminer  </TD><TD> <i>both</i> the boys  </TD>
</TR>
<TR>
<TD> POS  </TD><TD> possessive ending  </TD><TD> friend<i>'s</i>  </TD>

</TR>
<TR>
<TD> PRP  </TD><TD> personal pronoun  </TD><TD> I, he, it </TD>
</TR>
<TR>
<TD> PRP$  </TD><TD> possessive pronoun  </TD><TD> my, his </TD>

</TR>
<TR>
<TD> RB   </TD><TD> adverb   </TD><TD> however, usually, naturally, here, good </TD>
</TR>
<TR>
<TD> RBR   </TD><TD> adverb, comparative   </TD><TD> better</TD>

</TR>
<TR>
<TD> RBS   </TD><TD> adverb, superlative   </TD><TD> best </TD>
</TR>

<TR>
<TD> RP   </TD><TD> particle  </TD><TD> give <i> up </i></TD> 

</TR>
<TR>
<TD> TO  </TD><TD> to </TD><TD> <i>to</i> go, <i>to</i> him </TD> 
</TR>
<TR>

<TD> UH  </TD><TD> interjection  </TD><TD> uhhuhhuhh </TD> 
</TR>
<TR>
<TD> VB  </TD><TD> verb, base form  </TD><TD> take  </TD>

</TR>
<TR>
<TD> VBD </TD><TD>  verb, past tense   </TD><TD> took </TD>
</TR>
<TR>
<TD> VBG </TD><TD>verb, gerund/present participle</TD><TD> taking  </TD>

</TR>
<TR>
<TD> VBN  </TD><TD> verb, past participle    </TD><TD> taken     </TD>
</TR>
<TR>
<TD> VBP  </TD><TD> verb, sing. present, non-3d  </TD><TD>  take </TD> 

</TR>
<TR>
<TD> VBZ  </TD><TD> verb, 3rd person sing. present  </TD><TD> takes   </TD>
</TR>
<TR>
<TD> WDT   </TD><TD> wh-determiner    </TD><TD> which  </TD>
</TR>
<TR>
<TD> WP   </TD><TD> wh-pronoun  </TD><TD> who, what </TD>
</TR>
<TR>
<TD> WP$   </TD><TD> possessive wh-pronoun  </TD><TD> whose  </TD>
</TR>
<TR>
<TD> WRB   </TD><TD> wh-abverb  </TD><TD> where, when </TD>
</TR>
</TABLE>
</body>
</html>
