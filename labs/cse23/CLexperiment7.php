<html>
<style>
#earea
{
	    font-size: 16px;
background: #ffff00;

}
#earea span
{
cursor: pointer;
position: relative;
display: inline-block;
}

.act-span
{
}

.t-span
{
background: #00ff00;
padding: 0 2px 0 2px;
}
.f-span
{
background: #ff0000;
color: #fff;
padding: 0 2px 0 2px;
}
#ques {
position: absolute;
top: 15px;
left: -5px;
background: #e5e5e5;
display: none;
	 z-index: 10;
}
.act-span:hover
{
background:lightblue;
}
#ques ul li
{
list-style-type: none;
padding: 4px 5px;
border: 1px solid #111;
cursor: pointer;
color: #111;
}
#ques ul li:hover
{
background: #3b3131;
color: #FFF;
}

#flash
{
position: absolute;
top: 0;
left: 0;
color: #111;
background: #ff0;
padding: 8px 0px;
text-align: center;
width:100%;
      font-size: 17px;
display: none;
}

</style>



<script>
var ftimeout = null;
function showflash(msg)
{
clearTimeout(ftimeout);
$("#flash").text(msg).slideDown();
ftimeout = setTimeout(function () { $("#flash").slideUp(); }, 4000);

}

$("document").ready(function () {
$("#earea").children("span").addClass("act-span");

$(".act-span").hover(function (){
$("#ques").appendTo(this).css("display", "block");

	}, function (){
$("#ques").css("display", "none");
	});

$("#ques ul li").click(function (){
txt = $(this).text();
$par = $("#ques").parent();
ans = $par.attr("ans");
$("#ques").appendTo("body").css("display", "none");
if(txt == "Other" && (ans!="NN" && ans!="NNP"))
{
$par.removeClass().addClass("t-span");
obj = $par.text();
msg = "'" + obj + "' is a " + ans;
showflash(msg);
}
else if(txt!=ans)
{
$par.removeClass().addClass("f-span");
}
else
{
$par.removeClass().addClass("t-span");
}

	});

		});

</script>
<body>
<div id="flash"></div>
<div align="center">
[ Mark parts of speech for each word ]
<br />
<br />
</div>
<div id="earea">
<span ans="Adjective">India</span>
,
<span ans="NN">officially</span>
<span ans="Adjective">the</span>
<span ans="NN">Republic</span>
<span ans="Adjective">of</span>
<span ans="NNP">India</span>,
<span ans="Adjective">is</span>
<span ans="Adjective">a</span>
<span ans="NN">country</span>
<span ans="Adjective">in</span>
<span ans="NNP">South</span>
<span ans="NNP">Asia</span>.
<span ans="Adjective">It</span>
<span ans="Adjective">is</span>
<span ans="Adjective">the</span>
<span ans="Adjective">seventh</span>
-
<span ans="Adjective">largest</span>
<span ans="NN">country</span>
<span ans="Adjective">by</span>
<span ans="Adjective">geographical</span>
<span ans="Adjective">area</span>,
<span ans="Adjective">the</span>
<span ans="Adjective">second</span>-
<span ans="Adjective">most</span>
<span ans="Adjective">populous</span>
<span ans="NN">country</span>
<span ans="Adjective">with</span>
<span ans="Adjective">over</span>
<span ans="Adjective">1.2</span>
<span ans="Adjective">billion</span>
<span ans="NN">people</span>,
<span ans="Adjective">and</span>
<span ans="Adjective">the</span>
<span ans="Adjective">most</span>
<span ans="Adjective">populous</span>
<span ans="NN">democracy</span>
<span ans="Adjective">in</span>
<span ans="Adjective">the</span>
<span ans="Adjective">world</span>.
</div>
<div id="ques">
<ul>
<li>NN</li>
<li>NNP</li>
<li>Other</li>
</ul>
</div>
</body>
</html>
