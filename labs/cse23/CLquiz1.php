<html>
<HEAD>


<style type="text/css">
<!--
.bgclr {background-color: white; color: black; font-weight: bold;}
-->

</style>

<script language="JavaScript">

<!-- This script and many more are available free online at -->
<!-- The JavaScript Source!! http://javascript.internet.com -->

<!-- Begin
// Insert number of questions
var numQues = 6;

// Insert number of choices in each question
var numChoi = 8;

// Insert number of questions displayed in answer area
var answers = new Array(6);

// Insert answers to questions
answers[0] = "adverb";
answers[1] = "preposition";
answers[2] = "noun";
answers[3] = "conjunction";
answers[4] = "adjective";
answers[5] = "conjunction";

// Do not change anything below here ...
function getScore(form) {
  var score = 0;
  var currElt;
  var currSelection;
  for (i=0; i<numQues; i++) {
    currElt = i*numChoi;
    for (j=0; j<numChoi; j++) {
      currSelection = form.elements[currElt + j];
      if (currSelection.checked) {
        if (currSelection.value == answers[i]) {
          score++;
          break;
        }
      }
    }
  }
  score = Math.round(score/numQues*100);
  form.percentage.value = score + "%";
  var correctAnswers = "";
  for (i=1; i<=numQues; i++) {
    correctAnswers += i + ". " + answers[i-1] + "\r\n";
  }
  form.solutions.value = correctAnswers;
}
//  End -->
</script>

</HEAD>


<body>
Choose the correct part of speech from the list of the eight parts of speech to complete the question for the word surrounded by hyphens (- word -).<br><br>



<form name="quiz">
1. Unfortunately, they haven't been paying on time - recently -.
<ul style="margin-left:30px"><br>
  <li><input type="radio" name="q1" value="noun">noun</li>
 <li><input type="radio" name="q1" value="pronoun">pronoun</li>
  <li><input type="radio" name="q1" value="verb">verb</li>
  <li><input type="radio" name="q1" value="adverb">adverb</li>
  <li><input type="radio" name="q1" value="adjective">adjective</li>
  <li><input type="radio" name="q1" value="conjunction">conjunction</li>
  <li><input type="radio" name="q1" value="preposition">preposition</li>
  <li><input type="radio" name="q1" value="interjection">interjection</li>
  
</ul><br>

2. I'd like to thank you all - for - attending today's meeting.
<ul style="margin-left:30px"><br>
  <li><input type="radio" name="q2" value="noun">noun</li>
  <li><input type="radio" name="q2" value="pronoun">pronoun</li>
  <li><input type="radio" name="q2" value="verb">verb</li>
  <li><input type="radio" name="q2" value="adverb">adverb</li>
  <li><input type="radio" name="q2" value="adjective">adjective</li>
  <li><input type="radio" name="q2" value="conjunction">conjunction</li>
  <li><input type="radio" name="q2" value="preposition">preposition</li>
  <li><input type="radio" name="q2" value="interjection">interjection</li>
</ul><br>
3. Art lovers can visit the - Metropolitan Museum of Art - on the Upper Eastside next to Central Park.<br>
<ul style="margin-left:30px">
  <li><input type="radio" name="q3" value="noun">noun</li>
  <li><input type="radio" name="q3" value="pronoun">pronoun</li>
  <li><input type="radio" name="q3" value="verb">verb</li>
  <li><input type="radio" name="q3" value="adverb">adverb</li>
  <li><input type="radio" name="q3" value="adjective">adjective</li>
  <li><input type="radio" name="q3" value="conjunction">conjunction</li>
  <li><input type="radio" name="q3" value="preposition">preposition</li>
  <li><input type="radio" name="q3" value="interjection">interjection</li>
</ul><br>
4. However -, there are a number of places and events that really must be seen and enjoyed if you are in London.
<ul style="margin-left:30px"><br>
  <li><input type="radio" name="q4" value="noun">noun</li>
  <li><input type="radio" name="q4" value="pronoun">pronoun</li>
  <li><input type="radio" name="q4" value="verb">verb</li>
  <li><input type="radio" name="q4" value="adverb">adverb</li>
  <li><input type="radio" name="q4" value="adjective">adjective</li>
  <li><input type="radio" name="q4" value="conjunction">conjunction</li>
  <li><input type="radio" name="q4" value="preposition">preposition</li>
  <li><input type="radio" name="q4" value="interjection">interjection</li>
</ul><br>

5. To experience the famous - eccentric - side of British life, make sure to visit Speakers' Corner in Hyde Park
<ul style="margin-left:30px"><br>
  <li><input type="radio" name="q5" value="noun">noun</li>
  <li><input type="radio" name="q5" value="pronoun">pronoun</li>
  <li><input type="radio" name="q5" value="verb">verb</li>
  <li><input type="radio" name="q5" value="adverb">adverb</li>
  <li><input type="radio" name="q5" value="adjective">adjective</li>
  <li><input type="radio" name="q5" value="conjunction">conjunction</li>
  <li><input type="radio" name="q5" value="preposition">preposition</li>
  <li><input type="radio" name="q5" value="interjection">interjection</li>
</ul>
<br><br>
6. Not only are London parks beautiful, - but - they may be one of the only places you can meet the British.
<ul style="margin-left:30px">
 <li><input type="radio" name="q6" value="noun">noun</li>
  <li><input type="radio" name="q6" value="pronoun">pronoun</li>
  <li><input type="radio" name="q6" value="verb">verb</li>
  <li><input type="radio" name="q6" value="adverb">adverb</li>
  <li><input type="radio" name="q6" value="adjective">adjective</li>
  <li><input type="radio" name="q6" value="conjunction">conjunction</li>
  <li><input type="radio" name="q6" value="preposition">preposition</li>
  <li><input type="radio" name="q6" value="interjection">interjection</li>
</ul>
<br>
<input type="button" value="Get score" onClick="getScore(this.form)">
<input type="reset" value="Clear answers">
<p> Score = <strong><input class="bgclr" type="text" size="5" name="percentage" disabled></strong><br><br>
Correct answers:<br>
<textarea class="bgclr" name="solutions" wrap="virtual" rows="4" cols="30" disabled>
</textarea>
</form>
</body>
</html>