<html>
<body>
Translation of a word from one language (the source language) to another (the target language) brings along the problem of ambiguity. It is easy to see that a word in the source language based on its usage can end up getting translated in different ways on the target side. Take a following two examples in English:
<br><br>
(1) I go to the east-side bank every Sunday for birding.<br/>
(2) I went to the bank yesterday to sort out some financial issues.<br/>
<br>
Notice that the translation of the word 'bank' in the two sentence in Hindi will be different. This shows that ambiguous words in one language can take different form in the target language. The converse is also true. While building a dictionary these ambiguities must be captured as different 'sense' of the word. So, following the above example, the word 'bank' will appear with two sense in the dictionary.
<br><br>
Also linked to the notion of ambiguity is frequency. Recall the previous experiment. It has been found that in most languages, high frequency words are the most ambiguous. It is easy to see that including high frequency words in the dictionary is important. This will automatically lead to higher coverage of the dictionary.<br><br>
</body>
</html>
