<html>
<body>
A bi-lingual dictionary is an indispensable resource in the task of Machine Translation (MT). Availability of a high coverage bi-lingual dictionary can dramatically improve the quality of the translation. In Experiment 4 and 5 we discussed the notion of coverage and ambiguity of a word token and its relation to a dictionary. A typical dictionary entry for the word token 'bank' will be as follows:
<br><br>
<pre>
		  Token: bank
	       Sense id: bank%n%1
	Target language: Hindi
	   Target sense: किनारा
Target language example: mai roz subah nadi ke tat para
			 chidiayaa dekhne jaata hun.
Source language example: I go to the east-side bank every Sunday 
			 for birding.
		________________________
			 
		  Token: bank
	       Sense id: bank%n%2
	Target language: Hindi
	   Target sense: बैंक
Target language example: maine kal baika se paise nikaale
Source language example: I went to the bank yesterday to sort
			 out some financial issues.
</pre>
<br><br>

The value for each of attributes in an entry must be manually assigned. Notice that the value of sense id also provides the broad grained part of speech tag discusses in Experiments 1 and 2 earlier.<br>
</body>
</html>
