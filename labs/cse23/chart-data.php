<?php

$target_path="/CL/uploads/temp.txt";
$myfile=$target_path;
$fh=fopen($myfile,'r');

$theData="";
$wordCount=array();
$index=array();
$COUNT=0;

while(!feof($fh)){
	$theData = fgets($fh);
	$words=preg_split("/[:\s,]+/",$theData);
	for($i=0;$i<count($words);$i=$i+1){
		if($words[$i]!=""){
			$COUNT+=1;
			if(array_key_exists($words[$i],$wordCount)){
				$wordCount[$words[$i]]+=1;
			}
			else{
				$wordCount[$words[$i]]=1;
			}
		}
	}
}

fclose($fh);

sort($wordCount);

$Max=0;

foreach($wordCount as $key => $val){
	if(array_key_exists($val,$index)){
		$index[$val]=$index[$val]." ".$key;
	}
	else{
		$index[$val]=$key;
	}
	if($Max<$val){
		$Max=$val;
	}
}

$map=array();

foreach($index as $key => $val){
	$temp=preg_split("/[\s]+/",$val);
	$map[$key]=count($temp);
}

//the graph starts here...
//$map=$_GET['map'];
$th=(int)$_GET['th'];

include_once( 'open-flash-chart-1.9.7/php-ofc-library/open-flash-chart.php' );

if($th==-1){
	$g = new graph();
	$g->title( 'Frequency-Count' , '{font-size: 26px;}' );

	$arr=array_values($map);
	$bar = new bar_outline( 50, '#9933CC', '#8010A0' );
	$bar->data=$arr;
	
	$g->data_sets[]=$bar;

	$arr1=array_keys($map);
	$g->set_x_labels( $arr1 );
	$g->set_x_label_style(10,'#9933cc',0,1);
	$g->set_x_axis_steps(1);
	$g->set_x_legend('Frequency',12,'#736AFF');

	// set the Y axis...
	$g->set_y_max( $COUNT );
	$g->y_label_steps( 5 );
	$g->set_y_legend('Count',12,'#736AFF');

	echo $g->render();
}
else{

$covered=0;
$uncovered=0;
$C=0;
foreach($map as $key=>$val){
	if($key<$th){
		$uncovered+=$val;
	}
	else{
		$covered +=$val;
	}
	$C+=$val;
}
$h = new graph();

//
// PIE chart, 60% alpha
//
$h->pie(60,'#505050','{font-size: 12px; color: #404040;');
//
// pass in two arrays, one of data, the other data labels
//
$h->pie_values( array($covered*100/$C,$uncovered*100/$C), array('Covered','Uncovered') );
//
// Colours for each slice, in this case some of the colours
// will be re-used (3 colurs for 5 slices means the last two
// slices will have colours colour[0] and colour[1]):
//
$h->pie_slice_colours( array('#356aa0','#d01f3c') );

$h->set_tool_tip( '#val#%' );

$h->title('Coverage from total tokens of '.$C, '{font-size:18px; color: #d01f3c}' );

echo $h->render();
}

?>
