1	accounted	कुछ_होने_की_आशंका;मानना;निश्चित_मात्रा_में_होना	She never accounted that her child would fall ill during the trip.;A person is accounted minor until he/she is 18 years.;The Indian market accounted for 40% of the total sales.
2	ace	इक्का{ताश_का);कुशल;{टेनिस_में)सर्विस_जो_प्रतिद्वन्दी_नहीं_खेल_पाता	I just need an ace of diamond to win the game.;V.Anand is an ace chess player of India.;Roger Federer hit 21 aces in yesterday's match.
3	act	कार्य;नाटक_का_अंग;अधिनियम	He did his act three times every evening.;The murder of the King takes place in the first act itself.;Parliament has passed an act which makes such sports illegal.
4	add	जोड़ना;बढाना;मिलाना	If 2 is added to 3 we get 5.;Girish's irresponsible behavious adds to my worries.;When we add water to the paste we get a drink.
