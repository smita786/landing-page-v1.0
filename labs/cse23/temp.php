<?php
include_once( 'open-flash-chart-1.9.7/php-ofc-library/open-flash-chart.php' );

$bar = new bar_sketch( 50, 6, '#99FF00', '#7030A0' );
$bar->key( '% Complete', 10 );

$data = array();
$labels = array();

$bar->data[] = 50;
$labels[] = 'Mon';
$bar->data[] = 65;
$labels[] = 'Tue';
$bar->data[] = 70;
$labels[] = 'Wed';
$bar->data[] = 75;
$labels[] = 'Thu';
$bar->data[] = 80;
$labels[] = 'Fri';
$bar->data[] = 89;
$labels[] = 'Sat';
$bar->data[] = 100;
$labels[] = 'Sun';

//
// ----
//
$g = new graph();
$g->title( 'Paper Mario, % complete', '{font-size: 18px; color: #A0A0A0;}' );

$g->set_tool_tip( 'Progress, #val#%' );
//
//

// set the X axis labels
$g->set_x_labels( $labels );

//$g->set_data( $data );
//$g->bar_sketch( 50, 6, '#99FF00', '#7030A0', '% Complete', 10 );

//
// add the bar object to the graph
//
$g->data_sets[] = $bar;

$g->set_x_label_style( 10, '#A0A0A0', 0, 1 );
$g->set_y_label_style( 10, '#A0A0A0' );
$g->x_axis_colour( '#A0A0A0', '#FFFFFF' );
$g->set_x_legend( 'Week 1', 12, '#A0A0A0' );

$g->y_axis_colour( '#A0A0A0', '#FFFFFF' );


$g->set_y_min( 0 );
$g->set_y_max( 100 );
$g->y_label_steps( 2 );

$g->set_width( 400 );
$g->set_height( 250 );

$g->set_output_type('js');
echo $g->render();
?>
