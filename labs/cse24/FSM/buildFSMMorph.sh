#!/bin/sh
java textToFSM $1 $2
java morphGenerator $3

fsmdraw -F fig1.dot -i charSymbols.txt -o charSymbols.txt paradigm1_1.fst
dot -Tps -o fig1.ps fig1.dot
ps2pdf fig1.ps fig1.pdf
convert fig1.pdf fig1.jpg

fsmdraw -F fig2.dot -i charSymbols.txt -o charSymbols.txt paradigm1_2.fst
dot -Tps -o fig2.ps fig2.dot
ps2pdf fig2.ps fig2.pdf
convert fig2.pdf fig2.jpg

fsmdraw -F fig3.dot -i charSymbols.txt -o charSymbols.txt para1.fst
dot -Tps -o fig3.ps fig3.dot
ps2pdf fig3.ps fig3.pdf
convert fig3.pdf fig3.jpg

rm fig*.dot
rm fig*.pdf
#rm fig*.ps
