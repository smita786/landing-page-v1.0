<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<meta http-equiv="Content-Style-Type" content="text/css" />
<link rel="stylesheet" type="text/css" href="style.css" media="screen" />  
<script src="jquery.js"  type="text/javascript" ></script>

<script type="text/javascript">
var state="hidden";
function hide(layerId){
	var elem,vis;
	if(document.getElementById){
		elem=document.getElementByIdO(layerId);
	}
	else if(document.all){
		elem=document.all[layerId];
	}
	else if(document.layers){
		elem=document.layers[layerId];
	}
	vis=elem.styledisplay="none";

}	
function show(layerId){
//	hideAll();
	$('.divClass').fadeOut(100);
	$('#'+layerId).fadeIn(100);
//	document.getElementById(layerId).style.display="block";
}
/*function hideAll(){
	var myDiv = new Array();
	$('.divClass').fadeOut(200);
}*/
</script>
</head>
<body>
<script type="text/javascript">
$('document').ready(function (){

$('sidelink').click(function (){
$('.divs').css('display','none');
ids=this.id;
alert(ids);
$('#div'+ids).fadeIn(500);
	
});


});
</script>

<div id="wrap">
<div id="header">
<h1> Virtual Labs</h1>
<h2>Language Technologies Research Center</h2>
</div>
<div id="content">



<div class="menu">
<!--the right most-->

<h2>
<?php
$E=$_GET["Exp"];
$sel=$_GET["'sel"];
if ($E==1)
	echo "Experiment 1<br>";
elseif ($E==2)
	echo "Experiment 2<br>";
elseif ($E==3)
	echo "Experiment 3<br>";
?>
</h2>
<ul>
<li><a onclick="show('theory');" href="#" >Theory</a></li>
<li><a onclick="show('objective');" href="#" >Objective</a></li>
<li><a onclick="show('procedure');" href="#" >Procedure</a></li>
<li><a onclick="show('experiment');" href="#" >Experiment</a></li>
</ul>
</div>

<div class="right">
<!--the middle one-->
<div id="theory" class='divClass' >
<?php
if($E==1){
	echo "<h2 align=\"center\">Understanding analysis of words</h2><br>"; 
}
if($E==2){
	echo "<h2 align=\"center\">Understanding generation of words</h2><br>"; 
}
if($E==3){
	echo "<h2 align=\"center\">Encoding morphological information</h2><br>"; 
}
echo "<h3 align='center'> Theory</h3><br>";
$str="theory".$E.".php";
include($str);
?>
</div>
<div id="objective" class='divClass' style="display:none;">
<?php
if($E==1){
	echo "<h2 align=\"center\">Understanding analysis of words</h2><br>"; 
}
if($E==2){
	echo "<h2 align=\"center\">Understanding generation of words</h2><br>"; 
}
if($E==3){
	echo "<h2 align=\"center\">Encoding morphological information</h2><br>"; 
}
echo "<h3 align='center'> Objective</h3><br>";
$str="objective".$E.".php";
include($str);
?>
</div>
<div id="procedure" class='divClass' style="display:none;">
<?php
if($E==1){
	echo "<h2 align=\"center\">Understanding analysis of words</h2><br>"; 
}
if($E==2){
	echo "<h2 align=\"center\">Understanding generation of words</h2><br>"; 
}
if($E==3){
	echo "<h2 align=\"center\">Encoding morphological information</h2><br>"; 
}
echo "<h3 align='center'> Procedure</h3><br>";
$str="procedure".$E.".php";
include($str);
?>
</div>
<div id="experiment" class='divClass' style="display:none;">
<?php
if($E==1){
	echo "<h2 align=\"center\">Understanding analysis of words</h2><br>"; 
}
if($E==2){
	echo "<h2 align=\"center\">Understanding generation of words</h2><br>"; 
}
if($E==3){
	echo "<h2 align=\"center\">Encoding morphological information</h2><br>"; 
}
echo "<h3 align='center'> Experiment</h3><br>";
$str="experiment".$E.".php";
include($str);
?>
</div>
</div>

<div class="left">
<!--the left one-->
<div style="padding-left:35px;"><h2 ><a href="index.php">HOME</a></h2></div><br>
<h2 align="center" style="padding-right:20px;">NLP</h2>
<h2><a href="Exp.php?Exp=1">Experiment 1</a></h2>
<h2><a href="Exp.php?Exp=2">Experiment 2</a></h2>
<h2><a href="Exp.php?Exp=3">Experiment 3</a></h2>
<br><hr><br>

</div>
<div class='cleared'></div>
<?php
if($E==2){
}
if($E==3){
}
?>
</div>

<div id="bottom" style="padding-top:20px; padding-left:5px;">
<?
include('footer.php');
?>

</div>
</div>
</body>
</html>
