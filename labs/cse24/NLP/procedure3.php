<html>
<body>
<b><u>STEP1: </u></b>Select the language.<br>
<b><u>OUTPUT: </u></b>Another drop down to select script will appear in right. <br><br>
<b><u>STEP2: </u></b>Select the script.<br>
<b><u>OUTPUT: </u></b>Drop down to select root will appear below it.<br><br>
<b><u>STEP3: </u></b>Select the root from the drop down menu.<br>
<b><u>OUTPUT: </u></b>A complete feature table will appear for that root.<br>
Also an add-delete table will appear for the user to check his understanding of add-delete features.<br><br>
<b><u>STEP4: </u></b>Click on check to check your answers<br>
<b><u>OUTPUT: </u></b>The correct add delete table will appear besides the table <br><br>
<hr>
</body>
</html>
