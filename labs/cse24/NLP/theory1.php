<html>
<title>Word and its features</title>
<body>
<h3>Morphology</h3>
<br>
Morphology deals with words and its' features. Each word is associated with
a based word called 'root word'.<br>
For example, various word forms of a root word 'play' are, 'playing', 'played' 
 and 'plays'. Each word form has different values of features such root,
part of speech category, gender, number, person, etc.<br><br>

<div style="font-size:12px">
<b>Examples :</b>
<ul>
	<li>The features of an English word 'played' are,
	<ul>
		<li> root : play
		<li> category : verb
		<li> tense : past
	</ul>
	<li>The features of an Hindi word 'KelawI' (खेलती ) are,
	<ul>
		<li> root: KelanA (खेलना)
		<li> category : verb
		<li> tense : continious
		<li> gender : fem
		<li> number : singular
	</ul>
</ul>
<br>
<h3>Types of Morphology</h3><br>

Morphology is of two types,
<ol>
	<li> Inflectional morphology
	<p> 
    	Deals with word forms of a root, where there is no change
     	in lexical category. For example, 'played' is an inflection of the root
	word 'play'. Here, both 'played' and 'play' are verbs.
	</p>
	<li> Derivational morphology
	<p>
	Deals with word forms of a root, where there is a change in the
	lexical category. For example, the word form 'happiness' is a derivation
	of the word 'happy'. Here, 'happiness' is a derived noun form of the 
	adjective 'happy'.
	</p>
</ol>
<br>
<h3>Morphological Richness of various languages</h3><br>

Different languages have different levels of morphological richness i.e., a 
word form contains lot more features in some languages when compared to others.
Let's examine it by listing the inflectional word forms of verbal roots
in English, Hindi and Telugu.

<ul>
		<li> English
		<ul>
			<li> root: play
			<li> forms: play, plays, played, playing
		</ul>
		<li> Hindi
		<ul>
			<li> root: KelanA (खेलना)
			<li> forms: Kela, KelA, KelI, KeluMgA, KeluMgI, KelegA, 
				    KelegI, Kelawe, KelawI, KelanE, Kelakara<br>
			     (खेल, खेला, खेली, खेलुंगा,  खेलुंगी, खेलेगा, खेलेगी, खेलते, खेलती, खेलने, खेलकर)
		</ul>
		<li> Telugu
		<ul>
			<li> root: Adadam (ఆడడం)
			<li> forms: Adutaanu, AdutunnAnu, Adenu, Ademu, AdevA,
				AdutAru, Adutunnaru, AdadAniki, Adesariki, AdanA,
				Adinxi, Adutunxi, AdinxA, AdeserA, Adestunnaru ...
		</ul>
</ul>
</div>
<br>
<center>We can see that Indian languages are morphologically richer than English.</center>
<br>
<hr>
</body>
</html>
