<html>
<head>
<style type="text/css">
input.mybutton{
	font-size:14px;
	font-family:Trebuchet MS,sans-serif;
	font-weight:bold;
	color:#FFFFFF;
	height:19px;
       background-color:#003300;
       border-style:none;
}
input.myoptbutton{
	font-size:16px;
	font-family:Arial,sans-serif;
	height:26px;
       background-color:#779999;
       background-image:url(back03.gif);
       border-style:solid;
       border-color:#DDDDDD;
       border-width:1px;
}
</style>
</head>
<body>
<?php
$pos=$_GET['pos'];
echo '<br><center>Click one value from each column to generate word</center>';
if($pos=="noun"){
echo '
<br>
<table width="100%">
<tr>
<th style="font-size:14px;">Gender</th>
<th style="font-size:14px;">Number</th>
<th style="font-size:14px;">Reference</th>
</tr>
<tr>
<td>
<table><tr>
<td><input type=\'button\' id=\'male\' class=\'mybutton\' value=\'male\' onclick="setGender(\'m\');" /></td></tr><tr>
<td><input type=\'button\' id=\'female\' class=\'mybutton\' value=\'female\' onclick="setGender(\'f\');" /></td>
</tr></table>
</td>
<td>
<table><tr>
<td><input type=\'button\' id=\'singular\' class=\'mybutton\' value=\'singular\' onclick="setNumber(\'s\');" /></td></tr><tr>
<td><input type=\'button\' id=\'plural\' class=\'mybutton\' value=\'plural\' onclick="setNumber(\'p\');" /></td></tr></table>
</td>
<td>
<table>
<tr>
<td><input type=\'button\' id=\'direct\' class=\'mybutton\' value=\'direct\' onclick="setRef(\'d\');" /></td></tr><tr>
<td><input type=\'button\' id=\'oblique\' class=\'mybutton\' value=\'oblique\' onclick="setRef(\'o\');" /></td></tr>
</table>
</td>
</td></tr>
<tr>&nbsp</tr>
</table>
';
}
else if($pos="verb"){
echo'
<br>
<table width="100%">
<tr>
<th style="font-size:14px;">Gender</th>
<th style="font-size:14px;">Number</th>
<th style="font-size:14px;">Tense</th>
<th style="font-size:14px;">Person</th>
</tr>
<tr>
<td>
<table><tr>
<td><input type=\'button\' id=\'male\' class=\'mybutton\' value=\'male\' onclick="setGender(\'m\');" /></td></tr><tr>
<td><input type=\'button\' id=\'female\' class=\'mybutton\' value=\'female\' onclick="setGender(\'f\');" /></td>
</tr></table>
</td>
<td>
<table><tr>
<td><input type=\'button\' id=\'singular\' class=\'mybutton\' value=\'singular\' onclick="setNumber(\'s\');" /></td></tr><tr>
<td><input type=\'button\' id=\'plural\' class=\'mybutton\' value=\'plural\' onclick="setNumber(\'p\');" /></td></tr></table>
</td>
<td>
<table>
<tr>
<td><input type=\'button\' id=\'past\' class=\'mybutton\' value=\'simple-past\' onclick="setTense(\'spa\');" /></td></tr><tr>
<td><input type=\'button\' id=\'present\' class=\'mybutton\' value=\'simple-present\' onclick="setTense(\'spr\');" /></td></tr><tr>
<td><input type=\'button\' id=\'future\' class=\'mybutton\' value=\'simple-future\' onclick="setTense(\'sf\');" /></td></tr>
</table>
<td>
<table>
<tr>
<td><input type=\'button\' id=\'first\' class=\'mybutton\' value=\'first\' onclick="setPerson(\'f\');" /></td></tr><tr>
<td><input type=\'button\' id=\'second\' class=\'mybutton\' value=\'second\' onclick="setPerson(\'s\');" /></td></tr><tr>
<td><input type=\'button\' id=\'third\' class=\'mybutton\' value=\'third\' onclick="setPerson(\'t\');" /></td></tr>
</table>
</td></tr>
<tr>&nbsp</tr>
</table>
';

}
?>
<div style="padding-right:50px;":><center><input type='button' class='myoptbutton' value='Generate Word!!' onclick="getWord();"</center></div>

</body>
</html>
