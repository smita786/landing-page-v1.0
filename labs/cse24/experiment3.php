<html>
<head>
<script type="text/javascript" src='jquery.js'></script>
<script type="text/javascript">

function giveAnswer(){
	document.getElementById("answer").style.display='block';
}
function getFiller(root){
	if(root=='null'){
		err="<div align='center' style='padding-right:70px;'>Select root</div>";
		document.getElementById('filler').innerHTML=err;
		return;
	}
	$('#filler').load('exp3.php?index='+root);
}

function getOption(lang,scr){
	document.getElementById("screrr").innerHTML="";
	document.getElementById('option').innerHTML="";
	document.getElementById('filler').innerHTML="";
	if(scr=="null"){
		document.getElementById("screrr").innerHTML="select script";
		return;
	}
	$('#option').load('exp3_opt.php?lang='+lang+'&script='+scr);
}

function setValue(lang){
	document.getElementById("screrr").innerHTML="";
	document.getElementById('langerr').innerHTML="";
	document.getElementById('option').innerHTML="";
	document.getElementById('filler').innerHTML="";
	document.getElementById('script').innerHTML="";
	scr="<select name='script'>";
	scr=scr+"<option onclick='getOption(\"na\",\"null\");'>Select Script</option>";
	
	if(lang=='null'){
		document.getElementById('langerr').innerHTML="select languange";
		return;
	}
	else if(lang=='en'){
		scr=scr+"<option onclick='getOption(\"en\",\"roman\");'>Roman</option>";
		scr=scr+"</select>";
		document.getElementById('script').innerHTML=scr;
	}
	else if(lang=='hi'){
	//	scr=scr+"<option onclick='getOption(\"hi\",\"devanagiri\");'>Devanagiri</option>";
		scr=scr+"<option onclick='getOption(\"hi\",\"roman\");'>Roman</option>";
		scr=scr+"</select>";
		document.getElementById('script').innerHTML=scr;
	}
	
}

function getSelectedValue(){
	getdata(document.getElementById('root').value);
	document.getElementById('adddelete').style.visibility="visible";
	return false;
}
function getRoot(){
	return document.getElementById('root').value;
}
function getdata(ind){
		$('#fldiv').load('exp3.php?index='+ind);
}
function withRoot()
{
root=getRoot();
if(root=="null"){
	alert ("select one option");
	return;
}
url='exp3_1.php?index='+root;
$('#fform').attr('action',url);

}


</script>
</head>

<body>
<table width="100%">
<tr>
<td>
<select name="lang">
<option selected="selected" onclick="setValue('null');">Select language</option>
<option onclick="setValue('en');">English</option>
<option onclick="setValue('hi');">Hindi</option>
</select>
</td>
<td>
<div id="script"></div>
<tr>
<td><div id="langerr"></div></td>
<td><div id="screrr"></div></td>
</tr>
</td></tr>
</table>
<br>
<div id="option"></div>
<br>
<div id="filler"></div>
</body>
</html>
