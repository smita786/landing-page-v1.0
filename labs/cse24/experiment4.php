<html>
<head>
<script>
function showDig(){
	document.getElementById("dig").style.display="block";
}

</script>

</head>
<body>
<form action="generateFSM.php" method="post" target="FSM">
<table width="100%">
<tr>
<td>
<table cellspacing="-2" cellpadding="4" border="1" style="text-align:center;">
<tr>
<b>Fill the add delete table here:</b>
<br /><br/>
<th>Add</th>
<th>Delete</th>
<th>Number</th>
<th>Case</th>
</tr>
<?php
$N=array("sing","plu","sing","plu");
$C=array("dr","dr","ob","ob");
for($i=0;$i<4;$i++){
	echo "<tr>";
	echo "<td><input type=\"text\" style=\"width:30px; height:12px;\" name=\""."add".$N[$i].$C[$i]."\"></td>";
	echo "<td><input type=\"text\" style=\"width:30px; height:12px;\" name=\""."del".$N[$i].$C[$i]."\"></td>";
	echo "<td>".$N[$i]."</td>";
	echo "<td>".$C[$i]."</td>";
	echo "</tr>";
}
?>
</table>
</td>
<td>

<table cellspacing="-2" cellpadding="4" border="1" style="text-align:center;">
<tr>
<b>for example:</b>
<br><br>
<th>Add</th>
<th>Delete</th>
<th>Number</th>
<th>Case</th>
</tr>
<?php
//$N=array("sing","plu","sing","plu");
//$C=array("dr","dr","ob","ob");
$A=array("A","e","e","om");
$D=array("A","A","A","A");
for($i=0;$i<4;$i++){
	echo "<tr>";
	echo "<td>".$A[$i]."</td>";
	echo "<td>".$D[$i]."</td>";
	echo "<td>".$N[$i]."</td>";
	echo "<td>".$C[$i]."</td>";
	echo "</tr>";
}
?>
</table>

</td>
</tr>
</table>

<br>
<br>
<table width="100%">
<tr>
<td>
<table>
<b>Give paradigms (maximum : 4):</b><br/><br/>
<tr><td>paradigm1: <input type="text" style="width:100px; height:13px;" value="null" name="p1" /></tr></td>
<tr><td>paradigm2: <input type="text" style="width:100px; height:13px;" value="null" name="p2" /></tr></td>
<tr><td>paradigm3: <input type="text" style="width:100px; height:13px;" value="null" name="p3" /></tr></td>
<tr><td>paradigm4: <input type="text" style="width:100px; height:13px;" value="null" name="p4" /></tr></td>
</table>
</td>
<td>
<table>
<b>for Example:</b><br/><br/>
<tr><td>paradigm1: <input type="text" style="width:100px; height:13px;" value="laDak" disabled="true" /></tr></td>
<tr><td>paradigm2: <input type="text" style="width:100px; height:13px;" value="bacch" disabled="true" /></tr></td>
<tr><td>paradigm3: <input type="text" style="width:100px; height:13px;" value="null" disabled="true" /></tr></td>
<tr><td>paradigm4: <input type="text" style="width:100px; height:13px;" value="null" disabled="true" /></tr></td>
</table>


</td>
</tr>
</table>
<br/>
<!---<center><input type="submit" value="generate analyzer" onsubmit="showDig();" ></center>-->
<center><input type="submit" value="generate FSM" ></center>
</form>
<hr>
<center>
<a id="example1" href="FSM/paradigm1_add_delete_table.fst.txt"><button>Encoded file: add-delete</button></a>
<a id="example1" href="FSM/paradigm1_table.fst.txt"><button>Encoded file: paradigm-table</button></a>
</center>
<br />
<iframe name='FSM'  style="border-width:0px;width:450px;height:500px;overflow:visible;"></iframe>
<br>
</body>
</html>
