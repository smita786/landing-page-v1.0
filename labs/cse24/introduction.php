<html>
<body>
<h2 align='center'>Natural Language Processing Virtual Labs</h2>
<br>
Virtual Labs is an educational project of Ministry of Human Resource Development(HRD) of India to enable the creative minds of the nation to learn and apply their learning online. 
For more on <a target="_blank" href="http://www.vlab.co.in">Virtual Labs.</a>
<br>
<br>
<br>
<h3 align='center'>Relevant centres in India</h3>
<ol>
<li>IIT Bombay</li>
<li>Hyderabad Central University</li>
<li>IIIT Hyderabad</li>
<li>...</li>
</ol>
<br>
<h3 align='center'>LTRC</h3>
<br>
The centre conducts basic and applied research on various aspects of natural language 
technology. The focus is on developing technologies in three major areas: 
<ol>
<li>Language Access and Machine Translation among English and Indian languages </li>
<li>Speech Processing for Indian languages </li>
<li>Search, Information Extraction and Retrieval for English and Indian languages </li>
</ol>
</body>
</html>
