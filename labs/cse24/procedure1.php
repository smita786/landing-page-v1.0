<html>
<body>
<b><u>STEP1: </u></b>Select the language.<br>
<b><u>OUTPUT: </u></b>Another drop down to select script will appear in right. <br><br>
<b><u>STEP2: </u></b>Select the script.<br>
<b><u>OUTPUT: </u></b>Drop down to select word will appear below it.<br><br>
<b><u>STEP3: </u></b>Select the word from the drop down menu.<br>
<b><u>STEP4: </u></b>Click the "Analyse" button.<br>
<br>
<b><u>OUTPUT: </u></b>You will get the complete analysis of the selected word.
<br><br>
<hr>
</body>
</html>
