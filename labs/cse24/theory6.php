<html>
<body>
<h4>Steps for analyzing an input word</h4><br>
<ol>
<li> Build a single-path FSM for the input root and features.<br>
Example:<br>
<img src="./morph_images/analysis.jpg"  width="450px" style="margin-left:-20px;"/>
<br><br>
<li> Compose this FSM with FSM representing morphological generator. Then, project the output
words and remove epsilon symbols to get an FSM which represent the possible word.<br>
<img src="./morph_images/input-word.fsm.jpg" width="450px" style="margin-left:-20px;"/><br>
</ol>
<br>
<br>
<hr>
</body>
</html>
