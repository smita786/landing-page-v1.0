function Shift_Encrypt() {
    plaintext = document.getElementById("shift_plaintext").value.toLowerCase();  
    if(plaintext.length < 1){ alert("please enter some plaintext"); return; }    
    var shift = parseInt(document.getElementById("shift_key").value);
    ciphertext = "";    var re = /[a-z]/;
    for(i=0; i<plaintext.length; i++){ 
        if(re.test(plaintext.charAt(i))) ciphertext += String.fromCharCode((plaintext.charCodeAt(i) - 97 + shift)%26 + 97); 
        else ciphertext += plaintext.charAt(i); 
    } 
    document.getElementById("shift_ciphertext").value = ciphertext; 
} 
 
function Shift_Decrypt(f) { 
    ciphertext = document.getElementById("shift_ciphertext").value.toLowerCase();  
    // do some error checking 
    if(ciphertext.length < 1){ alert("please enter some ciphertext (letters only)"); return; }    
    var shift = parseInt(document.getElementById("shift_key").value);
    plaintext = "";    var re = /[a-z]/;
    for(i=0; i<ciphertext.length; i++){ 
        if(re.test(ciphertext.charAt(i))) plaintext += String.fromCharCode((ciphertext.charCodeAt(i) - 97 + 26 - shift)%26 + 97); 
        else plaintext += ciphertext.charAt(i); 
    } 
    document.getElementById("shift_plaintext").value = plaintext; 
}

var ciphers = ["haahjr ha khdu",
		"haajhr ha khdu"];
var current_cipher=0;
function Next_Shift_Test() {
    document.getElementById("textarea").value = ciphers[current_cipher];
    current_cipher = current_cipher+1;
    if (current_cipher > (ciphers.length-1)) {
	current_cipher=0;
    }
} 
