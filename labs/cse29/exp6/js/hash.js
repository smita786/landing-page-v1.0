// Author: Shikha Aggarwal

var current_l=4;

function validate_binary(input) {
    var len = input.length;
    var i;
    for(i=0;i<len;i++) {
	if(input.charAt(i) != "0" && input.charAt(i) != "1") {
	    break;
	}
    }
    if (i<len) {
	return 0;
    }
    return 1;
}

function isUnsignedInteger(s) {
  return (s.toString().search(/^[0-9]+$/) == 0);
}

function rand_sequence(len){
    ret="";
    for(i=0; i<len; i++) {
        ret += (Math.ceil(Math.random()*1000000))%2;
    }
    return ret;
}

function next_plain_text() {
    var len = Math.random()*100%100;
    document.getElementById("plaintext").value = rand_sequence(len);
}

function next_IV() {
    var l = document.getElementById("l").value;
    if (! isUnsignedInteger(l) ) {
	alert("l should be a positive integer");
	return;
    }
    var text_size = document.getElementById("plaintext").value.length;
    if (text_size > 4*l) {
    	current_l = l;
    } else {
	alert("l should not be greater than the (length of plaintext)/4");
        document.getElementById("l").value = current_l;
	return;
    }
    document.getElementById("iv").value = rand_sequence(current_l);
}

function XOR(a, b) {
    if((a == "0" && b == "0"))
        return "0";
    else if((a == "0" && b == "1"))
        return "1";
    else if((a == "1" && b == "0"))
        return "1";
    else if((a == "1" && b == "1"))
        return "0";
    return "0";
}

function get_hash() {
    var input = document.getElementById("usertext").value;
    if(validate_binary(input) == 0) {
	document.getElementById("usertext").value = "Please give a binary string of size " + 2*current_l;
	return;
    }
    var l = input.length;
    if(l != 2*current_l) {
	document.getElementById("usertext").value = "Please give a binary string of size 2l";
	return;
    }
    var output = "";
    for(var i=0;i<l/2;i++) {
	output += XOR(input.charAt(2*i), input.charAt(2*i+1));
    }
    document.getElementById("hashvalue").value = output;
}
