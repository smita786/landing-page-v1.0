var currentHours = 0;
var currentMinutes = 0;
var currentSeconds = 0;
currentMinutes = ( currentMinutes < 10 ? "0" : "" ) + currentMinutes;
currentHours = ( currentHours < 10 ? "0" : "" ) + currentHours;

$(document).ready(function() 
{
	
	
	var current_slide = 0;
	var num_slides = 2;
	
	$('#container').load('content.html', function(response,status,xhr)
    {
    	//alert("loaded successfully");
    	slides = $('#container > section');
		    
      	num_slides = slides.size();
      	//alert(num_slides);
      	$('#num-slides').html(num_slides);
      	$('#current-slide-no').html(current_slide + 1);
      	var slide = $(slides.get(0));
      	$("article.deck").append($("#container header"));
      	//$("article.deck").append("<div id='slides'> </div>");
      	$("article.deck").append("<div id='slides'></div>");
      	$("article.deck #slides").append(slide);
      	
      	
      	$("article.deck").append($("#container footer"));
      	$('#previous-slide').click(previous_slide);
      	$('#next-slide').click(next_slide);
      	$('#home-slide').click(home_slide);
      	$('#end-slide').click(end_slide);
      	$('html').bind('keydown', handle_keys);
      	
      	$("a.fancybox").fancybox(
		{
			'width'				: '90%',
			'height'			: '75%',
        	'autoScale'     	: false,
        	'transitionIn'		: 'none',
			'transitionOut'		: 'none',
			'type'				: 'iframe'
		});
		
		
    });	
	
	function set_current_slide(index)
	{
		var slide = $(slides.get(index));
		//alert(index);
		current_slide = index;
		$('#current-slide-no').html(current_slide + 1);
		//if(index == 0 || index == num_slides-1)
		//{
			//do nothing
		//}
		//else
			$("article.deck #slides section").replaceWith(slide);
			
	}
	
	function set_slide(offset) 
	{
		//increment slide with the correct offset
    		var index = current_slide + offset;
    		//alert(index);
    		if (index < 0) index = 0;
    		if (index > (num_slides-1)) index = (num_slides-1);
    		set_current_slide(index);
  	}

  	function previous_slide() 
	{
			if(current_slide > 0)
    			set_slide(-1);
    		return false;
  	}

  	function next_slide() 
	{
			//alert(current_slide);
			//alert(num_slides);
			if(current_slide < (num_slides-1))
    			set_slide(1);
    		return false;
  	}
  	
  	function home_slide()
  	{
  		set_current_slide(0);
  		return false;
  	}

	function end_slide()
  	{
  		set_current_slide(num_slides-1);
  		return false;
  	}

		
	function handle_keys(event) 
	{
    	/* Skip events with modifier keys */
    	if (event.altKey || event.ctrlKey || event.metaKey || event.shiftKey) 
		{
      			return true;
    	} 
		else 
		{
      			
			//Prevents the event from bubbling up the DOM tree, preventing any parent handlers from being notified of the event. 
      		event.stopPropagation();
			// If this method is called, the default action of the event will not be triggered. 
      		event.preventDefault();
    	}

    	switch (event.keyCode) 
		{
    		case $.ui.keyCode.HOME:
			//move to first slide
      			set_current_slide(0);
      			break;
    		case $.ui.keyCode.END:
				//move to last slide
      			set_current_slide(num_slides-1);
      			break;
    		case $.ui.keyCode.ENTER:
    		case $.ui.keyCode.SPACE:
    		case $.ui.keyCode.RIGHT:
    		case $.ui.keyCode.PAGEDOWN:
				//enter, space and right all navigate to next slide
      			next_slide();
      			break;
    		case $.ui.keyCode.LEFT:
    		case $.ui.keyCode.PAGEUP:
				// navigate to the previous slide
      			previous_slide();
      			break;
      		case 49: /* key 1 */
      			$('article.deck header').css('background', '#4C2916');
      			$('article.deck footer').css('background', '#4C2916');
      			break;
      		case 50: /* key 1 */
      			$('article.deck header').css('background', '#4863A0');
      			$('article.deck footer').css('background', '#4863A0');
      			     //$('header').addClass('theme2');
      			     //$('header').addClass('theme2');
      			break;
      		case 51: /* key 1 */
      			$('article.deck header').css('background', '#7E354D');
      			$('article.deck footer').css('background', '#7E354D');
      			     //$('header').addClass('theme2');
      			     //$('header').addClass('theme2');
      			break;
			case 52: /* key 1 */
      			$('article.deck header').css('background', '#667C26');
      			$('article.deck footer').css('background', '#667C26');
      			     //$('header').addClass('theme2');
      			     //$('header').addClass('theme2');
      			break;
			case 53: /* key 1 */
      			$('article.deck header').css('background', '#7E2217');
      			$('article.deck footer').css('background', '#7E2217');
      			     //$('header').addClass('theme2');
      			     //$('header').addClass('theme2');
      			break;

    		default:
      				break;
    		}
    		return false;
  	}
  	
	
	
	setInterval('updateClock()', 1000);
	
});

function updateClock ( )
	{
  	//var currentTime = new Date ( );

  	
  	//var cTime = currentTime.getTime();
  	//currentMinutes = ( currentMinutes < 10 ? "0" : "" ) + currentMinutes;
  	//var nTime = Math.round((cTime -sTime)/1000);
  	currentSeconds++;
  	//var nSecs= nTime.getSeconds();
  	if(currentSeconds >59)
  	{
  	currentMinutes++;
  	currentSeconds = 0;
  	currentMinutes = ( currentMinutes < 10 ? "0" : "" ) + currentMinutes;
  	}
  	if(currentMinutes >59)
  	{
  	currentHours++;
  	currentMinutes =0;
  	}
  	
  	
  	//alert(nTime);
  	//alert("Sec:"+ nSecs);
//alert(currentSeconds);alert(currentMinutes);alert(currentHours);
  	//alert(counterSet);
  		counterSet=1;
  	  	
	//alert(currentTimeString);
  	// Update the time display
  	
	// Pad the minutes and seconds with leading zeros, if required
	
	currentSeconds = ( currentSeconds < 10 ? "0" : "" ) + currentSeconds;
	
	// Compose the string for display
  	currentTimeString = currentHours + ":" + currentMinutes + ":" + currentSeconds;
	
  	$("#clock").html(currentTimeString);
  	  	
}
